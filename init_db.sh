#!/bin/bash
export PGUSER='user'
export PGPASSWORD='pass'

# Maximale Anzahl an Versuchen
max_attempts=5
# Wartezeit zwischen den Versuchen (in Sekunden)
wait_time=10

# Funktion, um die Datenbank zu erstellen
create_db() {
    psql -h postgres-db -d postgres -c "CREATE DATABASE $1;"
}

# Versuchen Sie, eine Verbindung zur Datenbank herzustellen
attempt=1
while ! psql -h postgres-db -d postgres -c '\q'; do
    echo "Warten auf die Datenbank... (Versuch $attempt von $max_attempts)"
    sleep $wait_time
    ((attempt++))
    if [ $attempt -gt $max_attempts ]; then
        echo "Maximale Anzahl an Verbindungsversuchen erreicht, Skript wird beendet."
        exit 1
    fi
done

# Datenbanken erstellen, wenn die Verbindung erfolgreich ist
create_db india
create_db mexico

unset PGUSER
unset PGPASSWORD
