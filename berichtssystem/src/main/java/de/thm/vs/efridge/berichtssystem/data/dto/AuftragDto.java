package de.thm.vs.efridge.berichtssystem.data.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuftragDto {

    private Long id;

    private ProduktDto produkt;

    private String status;

    private int anzahl;
}
