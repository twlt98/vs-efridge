package de.thm.vs.efridge.berichtssystem.service;

import de.thm.vs.efridge.berichtssystem.kafka.config.KafkaProperties;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.config.KafkaStreamsConfiguration;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BerichtsService {

    private final KafkaProperties kafkaProperties;

    /*@Bean("bestellungKStream")
    public KafkaStreams buildBestellungKafkaStream(
            @Qualifier("bestellungKStreamConfig") KafkaStreamsConfiguration kafkaStreamsConfiguration,
            StreamsBuilder streamsBuilder
    ) {
        // Definiere Stream Logic
        streamsBuilder.stream(kafkaProperties.topic().bestellung())
                .foreach((key, value) -> {
                    System.out.println("Received message - Key: " + key + ", Value: " + value);
                });

        // Erstelle den Kafka Stream
        KafkaStreams kafkaStreams = new KafkaStreams(streamsBuilder.build(), kafkaStreamsConfiguration.asProperties());

        // Hinzufügen eines Shutdown-Hooks, um den Stream ordnungsgemäß zu beenden
        Runtime.getRuntime().addShutdownHook(new Thread(kafkaStreams::close));

        // Starte den Stream
        kafkaStreams.start();

        return kafkaStreams;
    }*/

    @Bean("psStatusKStream")
    public KafkaStreams buildPsStatusKafkaStream(
            @Qualifier("psStatusKStreamConfig") KafkaStreamsConfiguration kafkaStreamsConfiguration,
            StreamsBuilder streamsBuilder
    ) {
        // Definiere Stream Logic
        streamsBuilder.stream(kafkaProperties.topic().produktionssteuerungStatus())
                .foreach((key, value) -> {
                    System.out.println("Received message - Key: " + key + ", Value: " + value);
                });

        // Erstelle den Kafka Stream
        KafkaStreams kafkaStreams = new KafkaStreams(streamsBuilder.build(), kafkaStreamsConfiguration.asProperties());

        // Hinzufügen eines Shutdown-Hooks, um den Stream ordnungsgemäß zu beenden
        Runtime.getRuntime().addShutdownHook(new Thread(kafkaStreams::close));

        // Starte den Stream
        kafkaStreams.start();

        return kafkaStreams;
    }
}

