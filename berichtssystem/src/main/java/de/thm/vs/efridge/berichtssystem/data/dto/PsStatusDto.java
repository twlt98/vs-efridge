package de.thm.vs.efridge.berichtssystem.data.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PsStatusDto {

    private String name;

    private Map<String, WerkStatusDto> werkStatusMap;

    private Map<String, List<AuftragDto>> werkAuftragMap;
}
