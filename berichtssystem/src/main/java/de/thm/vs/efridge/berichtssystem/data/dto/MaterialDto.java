package de.thm.vs.efridge.berichtssystem.data.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class MaterialDto {
    private Long id;
    private String name;
    private String beschreibung;
    private Long kategorieId;
    private double preis;
    private List<ProduktMaterialDto> produkte;
}