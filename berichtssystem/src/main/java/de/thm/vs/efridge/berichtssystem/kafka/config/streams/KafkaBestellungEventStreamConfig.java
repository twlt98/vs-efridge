package de.thm.vs.efridge.berichtssystem.kafka.config.streams;

import de.thm.vs.efridge.berichtssystem.kafka.config.KafkaProperties;
import de.thm.vs.efridge.berichtssystem.kafka.events.KafkaBestellungEvent;
import org.apache.kafka.common.serialization.*;
import org.apache.kafka.streams.StreamsConfig;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.KafkaStreamsConfiguration;
import org.springframework.kafka.support.mapping.DefaultJackson2JavaTypeMapper;
import org.springframework.kafka.support.mapping.Jackson2JavaTypeMapper;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class KafkaBestellungEventStreamConfig extends KafkaStreamConfig {

    public KafkaBestellungEventStreamConfig(KafkaProperties kafkaProperties) {
        super(kafkaProperties);
    }

    @Override
    @Bean(name = "bestellungKStreamConfig")
    public KafkaStreamsConfiguration kafkaStreamsConfiguration() {
        Map<String, Object> config = baseConfig();
        config.put(StreamsConfig.APPLICATION_ID_CONFIG, "berichtssystem-bestellung");
        config.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        config.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, KafkaBestellungEventSerde.class);
        return new KafkaStreamsConfiguration(config);
    }


    public static class KafkaBestellungEventSerde implements Serde<KafkaBestellungEvent> {
        @Override
        public Serializer<KafkaBestellungEvent> serializer() {
            return new JsonSerializer<>();
        }

        @Override
        public Deserializer<KafkaBestellungEvent> deserializer() {
            JsonDeserializer<KafkaBestellungEvent> deserializer = new JsonDeserializer<>(KafkaBestellungEvent.class);
            DefaultJackson2JavaTypeMapper typeMapper = new DefaultJackson2JavaTypeMapper();
            Map<String, Class<?>> mappings = new HashMap<>();
            typeMapper.setTypePrecedence(Jackson2JavaTypeMapper.TypePrecedence.INFERRED);
            mappings.put("KafkaBestellungEvent", KafkaBestellungEvent.class);
            typeMapper.setIdClassMapping(mappings);
            deserializer.setTypeMapper(typeMapper);
            return deserializer;
        }
    }
}