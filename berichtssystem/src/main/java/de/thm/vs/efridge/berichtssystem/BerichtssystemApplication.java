package de.thm.vs.efridge.berichtssystem;

import de.thm.vs.efridge.berichtssystem.kafka.config.KafkaProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@EnableConfigurationProperties(KafkaProperties.class)
public class BerichtssystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(BerichtssystemApplication.class, args);
    }

}
