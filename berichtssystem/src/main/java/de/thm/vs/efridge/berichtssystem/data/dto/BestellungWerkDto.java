package de.thm.vs.efridge.berichtssystem.data.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class BestellungWerkDto {
    private Long auftragId;
    private String status;
    private String werk;
    private Long produktId;
    private Long anzahl;
}
