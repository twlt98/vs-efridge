package de.thm.vs.efridge.berichtssystem.kafka.config.streams;

import de.thm.vs.efridge.berichtssystem.kafka.config.KafkaProperties;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.KafkaStreamsConfiguration;

import java.util.HashMap;
import java.util.Map;

@Configuration
@RequiredArgsConstructor
public abstract class KafkaStreamConfig {


    protected final KafkaProperties kafkaProperties;

    public Map<String, Object> baseConfig() {
        Map<String, Object> config = new HashMap<>();
        config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaProperties.bootstrapServers());
        return config;
    }

    public abstract KafkaStreamsConfiguration kafkaStreamsConfiguration();

    @Bean
    public StreamsBuilder kafkaStreamsBuilder() {
        return new StreamsBuilder();
    }
}
