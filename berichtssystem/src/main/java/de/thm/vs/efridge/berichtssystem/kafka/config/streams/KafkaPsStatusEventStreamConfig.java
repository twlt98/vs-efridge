package de.thm.vs.efridge.berichtssystem.kafka.config.streams;

import de.thm.vs.efridge.berichtssystem.kafka.config.KafkaProperties;
import de.thm.vs.efridge.berichtssystem.kafka.events.KafkaPsStatusEvent;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.kafka.streams.StreamsConfig;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.KafkaStreamsConfiguration;
import org.springframework.kafka.support.mapping.DefaultJackson2JavaTypeMapper;
import org.springframework.kafka.support.mapping.Jackson2JavaTypeMapper;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class KafkaPsStatusEventStreamConfig extends KafkaStreamConfig {

    public KafkaPsStatusEventStreamConfig(KafkaProperties kafkaProperties) {
        super(kafkaProperties);
    }


    @Override
    @Bean(name = "psStatusKStreamConfig")
    public KafkaStreamsConfiguration kafkaStreamsConfiguration() {
        Map<String, Object> config = baseConfig();
        config.put(StreamsConfig.APPLICATION_ID_CONFIG, "berichtssystem-psstatus");
        config.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        config.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, KafkaPsStatusEventSerde.class);
        return new KafkaStreamsConfiguration(config);
    }


    public static class KafkaPsStatusEventSerde implements Serde<KafkaPsStatusEvent> {
        @Override
        public Serializer<KafkaPsStatusEvent> serializer() {
            return new JsonSerializer<>();
        }

        @Override
        public Deserializer<KafkaPsStatusEvent> deserializer() {
            JsonDeserializer<KafkaPsStatusEvent> deserializer = new JsonDeserializer<>(KafkaPsStatusEvent.class);
            DefaultJackson2JavaTypeMapper typeMapper = new DefaultJackson2JavaTypeMapper();
            Map<String, Class<?>> mappings = new HashMap<>();
            typeMapper.setTypePrecedence(Jackson2JavaTypeMapper.TypePrecedence.INFERRED);
            mappings.put("KafkaPsStatusEvent", KafkaPsStatusEvent.class);
            typeMapper.setIdClassMapping(mappings);
            deserializer.setTypeMapper(typeMapper);
            return deserializer;
        }
    }
}