package de.thm.vs.efridge.berichtssystem.data.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProduktDto {
    private Long id;
    private String name;
    private String beschreibung;
    private double gewicht;
    private String dim;
    private Long produktionszeit;
    private List<ProduktMaterialDto> material;
}
