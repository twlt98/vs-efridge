package de.thm.vs.efridge.support.kafka.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "spring.kafka")
public record KafkaProperties(String bootstrapServers, KafkaConsumerProperties consumer, KafkaTopics topic) {

    public record KafkaConsumerProperties(String groupId) {
    }

    public record KafkaTopics(String support, String supportUpdateRequest, String supportUpdateCommit, String supportParticipants) {
    }
}
