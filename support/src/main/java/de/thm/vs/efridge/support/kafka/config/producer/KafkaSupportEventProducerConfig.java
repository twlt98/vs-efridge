package de.thm.vs.efridge.support.kafka.config.producer;

import de.thm.vs.efridge.support.kafka.config.KafkaProperties;
import de.thm.vs.efridge.support.kafka.events.KafkaSupportEvent;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KafkaSupportEventProducerConfig extends KafkaProducerConfig<KafkaSupportEvent> {
    public KafkaSupportEventProducerConfig(KafkaProperties kafkaProperties) {
        super(kafkaProperties);
    }
}
