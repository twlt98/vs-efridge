package de.thm.vs.efridge.support.repository;

import de.thm.vs.efridge.support.data.entities.SupportTicket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SupportTicketRepository extends JpaRepository<SupportTicket, Long> {

    Optional<SupportTicket> findByKundeId(Long kundeId);
}
