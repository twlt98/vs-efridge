package de.thm.vs.efridge.support.service;


import de.thm.vs.efridge.support.data.entities.SupportTicket;
import de.thm.vs.efridge.support.kafka.events.KafkaSupportEvent;
import de.thm.vs.efridge.support.mapper.SupportTicketMapper;
import de.thm.vs.efridge.support.repository.SupportTicketRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SupportTicketService {

    private final SupportTicketRepository supportTicketRepository;
    private final SupportTicketMapper supportTicketMapper;

    @ServiceActivator(inputChannel = "supportChannel")
    public void handleSupportTicket(KafkaSupportEvent kafkaSupportEvent) {
        System.out.println("Received Ticket: " + kafkaSupportEvent.toString());
        SupportTicket supportTicket = supportTicketMapper.dtoToSupportTicket(kafkaSupportEvent.getSupportTicket());
        supportTicket.setVersion(1L);
        supportTicket.setStatus("OPEN");
        addTickets(List.of(supportTicket));
    }

    public void addTickets(List<SupportTicket> supportTickets) {
        supportTicketRepository.saveAll(supportTickets);
    }

    public List<SupportTicket> retrieveAllTickets() {
        return supportTicketRepository.findAll();
    }

    public SupportTicket retrieveSupportTicketById(Long id) {
        return supportTicketRepository.findById(id).orElseThrow();
    }

    public SupportTicket retrieveSupportTicketByCustomer(Long id) {
        return supportTicketRepository.findByKundeId(id).orElseThrow();
    }

    public boolean checkUpdateRequest(SupportTicket supportTicket) {
        Optional<SupportTicket> toUpdate = supportTicketRepository.findById(supportTicket.getId());

        if (toUpdate.isEmpty()) {
            return false;
        }

        boolean canUpdate = !Objects.equals(toUpdate.get().getStatus(), "CLOSED") && Objects.equals(toUpdate.get().getVersion(), supportTicket.getVersion());

        if (canUpdate) {
            supportTicket.setVersion(supportTicket.getVersion()+1);
            supportTicketRepository.save(supportTicket);
        }

        return canUpdate;
    }

    public void updateTicket(SupportTicket supportTicket) {
        if (supportTicket.getId() == null || !supportTicketRepository.existsById(supportTicket.getId())) {
            // implement error
            return;
        }
        supportTicketRepository.save(supportTicket);
    }

}
