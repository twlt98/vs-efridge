package de.thm.vs.efridge.support.controller;

import de.thm.vs.efridge.support.data.dto.SupportTicketDto;
import de.thm.vs.efridge.support.data.entities.SupportTicket;
import de.thm.vs.efridge.support.mapper.SupportTicketMapper;
import de.thm.vs.efridge.support.service.SupportSyncService;
import de.thm.vs.efridge.support.service.SupportTicketService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.CompletableFuture;


@RestController
@RequestMapping("/support")
@RequiredArgsConstructor
public class SupportTicketController {

    private final SupportTicketService supportTicketService;
    private final SupportSyncService supportSyncService;
    private final SupportTicketMapper supportTicketMapper;

    @PutMapping("/{id}")
    public ResponseEntity<?> updateSupportTicket(@RequestBody SupportTicketDto supportTicketDto) {
        CompletableFuture<Boolean> updateFuture = supportSyncService.processUpdateRequest(supportTicketDto);
        try {
            Boolean updateSuccess = updateFuture.get(); // Warten auf das Ergebnis
            return updateSuccess ? ResponseEntity.ok().body("Update successful!") : ResponseEntity.badRequest().body("Update failed: Ticket already closed or old version. Please Refresh.");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error processing update");
        }
    }

    @GetMapping()
    public ResponseEntity<List<SupportTicketDto>> retrieveAllTickets() {
        List<SupportTicketDto> supportTicketDtos = supportTicketMapper.ticketsToDtos(supportTicketService.retrieveAllTickets());
        return ResponseEntity.ok(supportTicketDtos);
    }

    @GetMapping("/{id}")
    public ResponseEntity<SupportTicketDto> retrieveSupportTicketById(@PathVariable Long id) {
        SupportTicketDto supportTicketDto = supportTicketMapper.ticketToDto(supportTicketService.retrieveSupportTicketById(id));
        return ResponseEntity.ok(supportTicketDto);
    }

    @GetMapping("/kunde/{id}")
    public ResponseEntity<SupportTicketDto> retrieveSupportTicketByCustomer(@PathVariable Long id) {
        SupportTicketDto supportTicketDto = supportTicketMapper.ticketToDto(supportTicketService.retrieveSupportTicketByCustomer(id));
        return ResponseEntity.ok(supportTicketDto);
    }

}
