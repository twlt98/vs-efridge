package de.thm.vs.efridge.support.mapper;

import de.thm.vs.efridge.support.data.dto.SupportTicketDto;
import de.thm.vs.efridge.support.data.entities.SupportTicket;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface SupportTicketMapper {

    SupportTicketDto ticketToDto(SupportTicket ticket);

    List<SupportTicketDto> ticketsToDtos(List<SupportTicket> tickets);

    SupportTicket dtoToSupportTicket(SupportTicketDto ticketDto);

    List<SupportTicket> dtosToSupportTickets(List<SupportTicketDto> ticketDtos);
}
