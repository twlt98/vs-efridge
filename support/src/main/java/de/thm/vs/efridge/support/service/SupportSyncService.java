package de.thm.vs.efridge.support.service;

import de.thm.vs.efridge.support.data.dto.SupportTicketDto;
import de.thm.vs.efridge.support.data.entities.SupportTicket;
import de.thm.vs.efridge.support.kafka.config.KafkaProperties;
import de.thm.vs.efridge.support.kafka.events.KafkaSupportEvent;
import de.thm.vs.efridge.support.kafka.producer.SupportEventProducerService;
import de.thm.vs.efridge.support.mapper.SupportTicketMapper;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Service
@RequiredArgsConstructor
public class SupportSyncService {

    private final SupportTicketService supportTicketService;

    private final SupportTicketMapper supportTicketMapper;

    private final KafkaProperties kafkaProperties;

    private final SupportEventProducerService supportEventProducerService;

    private  final KafkaTemplate<String, String> kafkaTemplate;
    private final ConcurrentHashMap<String, CompletableFuture<Boolean>> futures = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<String, Integer> responseCounters = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<String, Boolean> updateDecisions = new ConcurrentHashMap<>();
    private final Set<String> totalParticipants = new HashSet<>();

    @PostConstruct
    public void publishInitialSignal() {
        kafkaTemplate.send(kafkaProperties.topic().supportParticipants(), kafkaProperties.consumer().groupId());
    }

    @Scheduled(fixedRate = 30000) // 30.000 Millisekunden = 30 Sekunden
    public void clearSet() {
        totalParticipants.clear();
        System.out.println("Teilnehmer wurden zurückgesetzt.");
    }

    @Scheduled(fixedRate = 2000) // 20.000 Millisekunden = 30 Sekunden
    public void publishSignal() {
        kafkaTemplate.send(kafkaProperties.topic().supportParticipants(), kafkaProperties.consumer().groupId());
    }

    @ServiceActivator(inputChannel = "supportParticipantsChannel")
    public void handleSupportParticitpantRequest(String groupId) {
        String selfId = kafkaProperties.consumer().groupId();
        groupId = groupId.replace("\"", "");
        if (!Objects.equals(groupId, selfId) && !totalParticipants.contains(groupId)) {
            System.out.println("Teilnehmer hinzugefügt: " + groupId);
            totalParticipants.add(groupId);
        }
    }

    @ServiceActivator(inputChannel = "supportUpdateRequestChannel")
    public void handleSupportUpdateRequest(KafkaSupportEvent kafkaSupportEvent) {
        String selfId = kafkaProperties.consumer().groupId();
        String groupId = kafkaSupportEvent.getGroupId().replace("\"", "");
        if (!Objects.equals(groupId, selfId)) {
            System.out.println("Received Update Request: " + kafkaSupportEvent);

            SupportTicket supportTicket = supportTicketMapper.dtoToSupportTicket(kafkaSupportEvent.getSupportTicket());
            boolean canUpdate = supportTicketService.checkUpdateRequest(supportTicket);

            supportEventProducerService.publishSupportUpdateCommit(supportTicket, kafkaSupportEvent.getId(), canUpdate);
        }
    }

    @ServiceActivator(inputChannel = "supportUpdateCommitChannel")
    public void handleSupportUpdateCommit(KafkaSupportEvent kafkaSupportEvent) {
        String selfId = kafkaProperties.consumer().groupId();
        String groupId = kafkaSupportEvent.getGroupId().replace("\"", "");
        if (!Objects.equals(groupId, selfId)) {
            System.out.println("Received Commit: " + kafkaSupportEvent);
            String requestId = kafkaSupportEvent.getId();
            responseCounters.computeIfPresent(requestId, (key, count) -> count + 1);
            if (!kafkaSupportEvent.isCanUpdate()) {
                updateDecisions.put(requestId, false);
            }
            checkAndUpdateIfReady(kafkaSupportEvent);
        }

    }

    public CompletableFuture<Boolean> processUpdateRequest(SupportTicketDto supportTicketDto) {
        CompletableFuture<Boolean> updateFuture = new CompletableFuture<>();

        String requestId = UUID.randomUUID().toString();
        futures.put(requestId, updateFuture);

        sendUpdateRequest(supportTicketDto, requestId); // Sendet Kafka-Nachricht

        Executors.newSingleThreadScheduledExecutor().schedule(() -> {
            // Überprüfen, ob alle Antworten erhalten wurden
            if (responseCounters.getOrDefault(requestId, 0) == totalParticipants.size()) {
                boolean success = updateDecisions.getOrDefault(requestId, false);
                updateFuture.complete(success); // Setzt das Ergebnis des Futures
            } else {
                updateFuture.completeExceptionally(new RuntimeException("Not all responses received"));
            }
            cleanup(requestId);
        }, 5, TimeUnit.SECONDS); // Warten Sie z.B. 5 Sekunden auf alle Antworten

        return updateFuture;
    }


    public void sendUpdateRequest(SupportTicketDto supportTicketDto, String requestId) {
        responseCounters.put(requestId, 0);
        updateDecisions.put(requestId, true); // Annahme: Update ist zunächst erlaubt

        SupportTicket supportTicket = supportTicketMapper.dtoToSupportTicket(supportTicketDto);
        supportEventProducerService.publishSupportUpdateRequest(supportTicket, requestId);
    }


    private void checkAndUpdateIfReady(KafkaSupportEvent response) {
        String requestId = response.getId();
        CompletableFuture<Boolean> future = futures.get(requestId);
        if (future != null && responseCounters.getOrDefault(requestId, 0) == totalParticipants.size()) {
            boolean success = updateDecisions.getOrDefault(requestId, false);
            future.complete(success);

            if (success) {
                SupportTicket supportTicket = supportTicketMapper.dtoToSupportTicket(response.getSupportTicket());
                supportTicketService.updateTicket(supportTicket);
            }

            cleanup(requestId);
        }
    }

    private void cleanup(String requestId) {
        futures.remove(requestId);
        responseCounters.remove(requestId);
        updateDecisions.remove(requestId);
    }
}
