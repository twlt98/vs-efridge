package de.thm.vs.efridge.support.kafka.events;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
public class KafkaEvent {
    protected String id;
    protected String operationType;
    protected String groupId;
}
