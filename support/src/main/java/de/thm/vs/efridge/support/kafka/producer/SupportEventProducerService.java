package de.thm.vs.efridge.support.kafka.producer;

import de.thm.vs.efridge.support.data.entities.SupportTicket;
import de.thm.vs.efridge.support.kafka.config.KafkaProperties;
import de.thm.vs.efridge.support.kafka.events.KafkaSupportEvent;
import de.thm.vs.efridge.support.mapper.SupportTicketMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.UUID;


@Service
@RequiredArgsConstructor
public class SupportEventProducerService {

    private final KafkaProperties kafkaProperties;
    private final KafkaTemplate<String, KafkaSupportEvent> kafkaTemplate;
    private final SupportTicketMapper supportTicketMapper;

    public void publishSupportUpdateRequest(SupportTicket supportTicket, String requestId) {
        kafkaTemplate.send(kafkaProperties.topic().supportUpdateRequest(), requestId,
                KafkaSupportEvent.builder()
                        .id(requestId)
                        .operationType("")
                        .groupId(kafkaProperties.consumer().groupId())
                        .supportTicket(supportTicketMapper.ticketToDto(supportTicket))
                        .build());
    }

    public void publishSupportUpdateCommit(SupportTicket supportTicket, String requestId, boolean canUpdate) {
        String uuid = UUID.randomUUID().toString();
        kafkaTemplate.send(kafkaProperties.topic().supportUpdateCommit(), uuid,
                KafkaSupportEvent.builder()
                        .id(requestId)
                        .operationType("")
                        .canUpdate(canUpdate)
                        .groupId(kafkaProperties.consumer().groupId())
                        .supportTicket(supportTicketMapper.ticketToDto(supportTicket))
                        .build());
    }
}
