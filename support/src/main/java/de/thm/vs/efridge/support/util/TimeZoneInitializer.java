package de.thm.vs.efridge.support.util;

import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class TimeZoneInitializer {

    @Value("${app.timezone}")
    private String timezone;

    @PostConstruct
    public void init() {
        LocalDateTimeConverter.setZoneId(timezone);
    }
}
