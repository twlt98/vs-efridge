package de.thm.vs.efridge.support.kafka.events;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import de.thm.vs.efridge.support.data.dto.SupportTicketDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonTypeName(value = "KafkaSupportEvent")
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
public class KafkaSupportEvent extends KafkaEvent {

    private SupportTicketDto supportTicket;

    private boolean canUpdate;

    private LocalDateTime timeStamp;

}
