package de.thm.vs.efridge.support.data.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SupportTicketDto {
    private Long id;

    private String beschreibung;

    private String kommentar;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

    private String status;

    private Long kundeId;

    private Long version;
}
