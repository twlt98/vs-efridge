package de.thm.vs.efridge.support.kafka.config;

import de.thm.vs.efridge.support.kafka.events.KafkaSupportEvent;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.kafka.dsl.Kafka;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;

import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableIntegration
@RequiredArgsConstructor
public class KafkaIntegrationConfig {

    private final KafkaProperties kafkaProperties;

    private final ConsumerFactory<String, KafkaSupportEvent> supportConsumerFactory;

    @Bean
    public IntegrationFlow supportFlow() {
        return IntegrationFlow
                .from(Kafka.messageDrivenChannelAdapter(supportConsumerFactory,
                        kafkaProperties.topic().support()))
                .channel("supportChannel")
                .get();
    }

    @Bean
    public IntegrationFlow supportUpdateRequestFlow() {
        return IntegrationFlow
                .from(Kafka.messageDrivenChannelAdapter(supportConsumerFactory,
                        kafkaProperties.topic().supportUpdateRequest()))
                .channel("supportUpdateRequestChannel")
                .get();
    }

    @Bean
    public IntegrationFlow supportUpdateCommitFlow() {
        return IntegrationFlow
                .from(Kafka.messageDrivenChannelAdapter(supportConsumerFactory,
                        kafkaProperties.topic().supportUpdateCommit()))
                .channel("supportUpdateCommitChannel")
                .get();
    }

    @Bean
    public IntegrationFlow supportParticipantsFlow() {
        return IntegrationFlow
                .from(Kafka.messageDrivenChannelAdapter(new DefaultKafkaConsumerFactory<String, String>(config()),
                        kafkaProperties.topic().supportParticipants()))
                .channel("supportParticipantsChannel")
                .get();
    }

    public Map<String, Object> config() {
        Map<String, Object> conf = new HashMap<>();
        conf.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaProperties.bootstrapServers());
        conf.put(ConsumerConfig.GROUP_ID_CONFIG, kafkaProperties.consumer().groupId());
        conf.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        conf.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        return conf;
    }
}
