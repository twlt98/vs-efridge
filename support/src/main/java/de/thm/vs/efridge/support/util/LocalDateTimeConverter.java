package de.thm.vs.efridge.support.util;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;
import org.springframework.stereotype.Component;

@Converter
@Component
public class LocalDateTimeConverter implements AttributeConverter<LocalDateTime, ZonedDateTime> {

    private static ZoneId ZONE_ID;

    public static void setZoneId(String zoneId) {
        ZONE_ID = ZoneId.of(zoneId);
    }

    @Override
    public ZonedDateTime convertToDatabaseColumn(LocalDateTime localDateTime) {
        return localDateTime == null ? null : ZonedDateTime.of(localDateTime, ZONE_ID);
    }

    @Override
    public LocalDateTime convertToEntityAttribute(ZonedDateTime zonedDateTime) {
        return zonedDateTime == null ? null : zonedDateTime.toLocalDateTime();
    }
}
