package de.thm.vs.efridge.support;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
class SupportApplicationTests {

	@Test
	void contextLoads() {
	}

}
