package de.thm.vs.efridge.auftragsverarbeitung.service;

import de.thm.vs.efridge.auftragsverarbeitung.data.entities.Kunde;
import de.thm.vs.efridge.auftragsverarbeitung.repository.KundeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class KundeService {

    private final KundeRepository kundeRepository;

    public Kunde addKunde(Kunde kunde) {
        return kundeRepository.save(kunde);
    }

    public List<Kunde> addKunden(List<Kunde> auftraege) {
        return kundeRepository.saveAll(auftraege);
    }

    public Kunde updateKunde(Kunde kunde) {
        if (kunde.getId() == null || !kundeRepository.existsById(kunde.getId())) {
            // implement error
            return null;
        }
        return kundeRepository.save(kunde);
    }

    public void deleteKundeById(Long id) {
        if (!kundeRepository.existsById(id)) {
            // implement error
        }
        kundeRepository.deleteById(id);
    }

    public List<Kunde> retrieveAllKunden() {
        return kundeRepository.findAll();
    }

    public Kunde retrieveKundeById(Long id) {
        return kundeRepository.findById(id).orElseThrow();
    }

}
