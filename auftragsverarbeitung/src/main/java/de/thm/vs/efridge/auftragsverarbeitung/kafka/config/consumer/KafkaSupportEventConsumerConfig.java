package de.thm.vs.efridge.auftragsverarbeitung.kafka.config.consumer;

import de.thm.vs.efridge.auftragsverarbeitung.kafka.config.KafkaProperties;
import de.thm.vs.efridge.auftragsverarbeitung.kafka.events.KafkaSupportEvent;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.mapping.DefaultJackson2JavaTypeMapper;
import org.springframework.kafka.support.mapping.Jackson2JavaTypeMapper;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.HashMap;
import java.util.Map;

@EnableKafka
@Configuration
public class KafkaSupportEventConsumerConfig extends KafkaConsumerConfig<KafkaSupportEvent>{


    public KafkaSupportEventConsumerConfig(KafkaProperties kafkaProperties) {
        super(kafkaProperties);
    }


    @Override
    @Bean("supportEventKafkaConsumerFactory")
    public ConsumerFactory<String, KafkaSupportEvent> kafkaConsumerFactory() {
        JsonDeserializer<KafkaSupportEvent> deserializer = new JsonDeserializer<>(KafkaSupportEvent.class);
        DefaultJackson2JavaTypeMapper typeMapper = new DefaultJackson2JavaTypeMapper();
        Map<String, Class<?>> mappings = new HashMap<>();
        typeMapper.setTypePrecedence(Jackson2JavaTypeMapper.TypePrecedence.INFERRED);
        mappings.put("KafkaSupportEvent", KafkaSupportEvent.class);
        typeMapper.setIdClassMapping(mappings);
        deserializer.setTypeMapper(typeMapper);
        deserializer.addTrustedPackages("*");
        return new DefaultKafkaConsumerFactory<>(config(), new StringDeserializer(), deserializer);
    }


    @Override
    @Bean("supportEventKafkaListenerContainerFactory")
    public ConcurrentKafkaListenerContainerFactory<String, KafkaSupportEvent> kafkaListenerContainerFactory(
            @Qualifier("supportEventKafkaConsumerFactory") ConsumerFactory<String, KafkaSupportEvent> kafkaConsumerFactory) {
        ConcurrentKafkaListenerContainerFactory<String, KafkaSupportEvent> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(kafkaConsumerFactory());
        return factory;
    }
}
