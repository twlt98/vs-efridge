package de.thm.vs.efridge.auftragsverarbeitung.mapping;


import de.thm.vs.efridge.auftragsverarbeitung.data.dto.KundeDto;
import de.thm.vs.efridge.auftragsverarbeitung.data.entities.Kunde;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface KundeMapper {

    KundeDto kundeToDto(Kunde kunde);

    List<KundeDto> kundenToDtos(List<Kunde> kunden);

    Kunde dtoToKunde(KundeDto kundeDto);

    List<Kunde> dtosToKunden(List<KundeDto> kundeDtos);
}
