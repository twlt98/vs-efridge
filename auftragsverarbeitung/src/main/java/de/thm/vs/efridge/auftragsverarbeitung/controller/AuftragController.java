package de.thm.vs.efridge.auftragsverarbeitung.controller;

import de.thm.vs.efridge.auftragsverarbeitung.data.dto.AuftragDto;
import de.thm.vs.efridge.auftragsverarbeitung.data.entities.Auftrag;
import de.thm.vs.efridge.auftragsverarbeitung.mapping.AuftragMapper;
import de.thm.vs.efridge.auftragsverarbeitung.service.AuftragService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/auftraege")
@RequiredArgsConstructor
public class AuftragController {

    private final AuftragService auftragService;
    private final AuftragMapper auftragMapper;

    @PostMapping()
    public ResponseEntity<?> addAuftraege(@RequestBody List<AuftragDto> auftragDtos) {
        // converts dto objects in Auftrag entities
        List<Auftrag> auftraege = auftragMapper.dtosToAuftraege(auftragDtos);

        try {
            auftragDtos = auftragMapper.auftraegeToDtos(auftragService.addAuftraege(auftraege));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }

        return ResponseEntity.ok(auftragDtos);
    }

    @GetMapping()
    public ResponseEntity<List<AuftragDto>> retrieveAllAuftraege() {
        // gets Auftraege from system and converts them to dtos
        List<AuftragDto> auftragDtos = auftragMapper.auftraegeToDtos(auftragService.retrieveAllAuftraege());
        return ResponseEntity.ok(auftragDtos);
    }

    /**
     * Retrieves Auftrag by id
     * @param id
     */
    @GetMapping("/{id}")
    public ResponseEntity<AuftragDto> retrieveAuftragById(@PathVariable Long id) {
        // gets Auftrag with given id from system and converts it to dto
        AuftragDto auftragDto = auftragMapper.auftragToDto(auftragService.retrieveAuftragById(id));
        return ResponseEntity.ok(auftragDto);
    }

    /**
     * Retrieves Auftrag by Customer with id
     * @param id
     */
    @GetMapping("/kunde/{id}")
    public ResponseEntity<AuftragDto> retrieveAuftragByCustomer(@PathVariable Long id) {
        // gets Auftrag from system with given customer id, converts it to dto
        AuftragDto auftragDto = auftragMapper.auftragToDto(auftragService.retrieveAuftragByCustomer(id));
        return ResponseEntity.ok(auftragDto);
    }

}
