package de.thm.vs.efridge.auftragsverarbeitung;

import de.thm.vs.efridge.auftragsverarbeitung.kafka.config.KafkaProperties;
import de.thm.vs.efridge.auftragsverarbeitung.kafka.repository.EventRepositoryFactoryBean;
import de.thm.vs.efridge.auftragsverarbeitung.kafka.repository.EventRepositoryImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableConfigurationProperties(KafkaProperties.class)
@EnableJpaRepositories(repositoryBaseClass = EventRepositoryImpl.class,
		repositoryFactoryBeanClass = EventRepositoryFactoryBean.class)
public class AuftragsverarbeitungApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuftragsverarbeitungApplication.class, args);
	}

}
