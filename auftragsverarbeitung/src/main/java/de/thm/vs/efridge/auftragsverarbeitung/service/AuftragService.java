package de.thm.vs.efridge.auftragsverarbeitung.service;

import de.thm.vs.efridge.auftragsverarbeitung.data.dto.AuftragDto;
import de.thm.vs.efridge.auftragsverarbeitung.data.entities.Auftrag;
import de.thm.vs.efridge.auftragsverarbeitung.kafka.events.KafkaAuftragEvent;
import de.thm.vs.efridge.auftragsverarbeitung.kafka.producer.AuftragEventProducerService;
import de.thm.vs.efridge.auftragsverarbeitung.mapping.AuftragMapper;
import de.thm.vs.efridge.auftragsverarbeitung.repository.AuftragRepository;
import de.thm.vs.efridge.auftragsverarbeitung.repository.KundeRepository;
import de.thm.vs.efridge.auftragsverarbeitung.repository.ProduktRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AuftragService {

    private final AuftragRepository auftragRepository;

    private final ProduktRepository produktRepository;

    private final KundeRepository kundeRepository;

    private final AuftragMapper auftragMapper;

    private final AuftragEventProducerService auftragEventProducerService;

    public List<Auftrag> addAuftraege(List<Auftrag> auftraege) throws Exception {

        for (Auftrag a : auftraege) {
            if (!kundeRepository.existsById(a.getKunde().getId())) {
                throw new Exception("Kunde existiert nicht!");
            }
            if (!produktRepository.existsById(a.getProdukt().getId())) {
                throw new Exception("Produkt existiert nicht!");
            }
        }

        return auftragRepository.saveAllAndPublish(auftraege);
    }


    public List<Auftrag> retrieveAllAuftraege() {
        return auftragRepository.findAll();
    }

    public Auftrag retrieveAuftragById(Long id) {
        return auftragRepository.findById(id).orElseThrow();
    }

    public Auftrag retrieveAuftragByCustomer(Long id) {
        return auftragRepository.findByKundeId(id).orElseThrow();
    }

    @Scheduled(fixedRate = 30000) // 30.000 Millisekunden = 30 Sekunden
    public void clearSet() {
        System.out.println("RETRY für zurückgestellte Aufträge.");
        List<Auftrag> zurueckgestellteAuftraege = auftragRepository.findByStatus("RETRY");
        for(Auftrag a : zurueckgestellteAuftraege) {
            auftragEventProducerService.publishAuftrag(a);
        }
    }

    @ServiceActivator(inputChannel = "auftragChannel")
    public void handleAuftrag(KafkaAuftragEvent kafkaAuftragEvent) {
        // gets Auftrag and converts to dto
        AuftragDto auftragDto = kafkaAuftragEvent.getAuftrag();
        // converts back to Auftrag
        Auftrag auftrag = auftragMapper.dtoToAuftrag(auftragDto);
        // saves Auftrag
        auftragRepository.save(auftrag);
    }
}
