package de.thm.vs.efridge.auftragsverarbeitung.controller;


import de.thm.vs.efridge.auftragsverarbeitung.data.dto.KundeDto;
import de.thm.vs.efridge.auftragsverarbeitung.data.entities.Kunde;
import de.thm.vs.efridge.auftragsverarbeitung.mapping.KundeMapper;
import de.thm.vs.efridge.auftragsverarbeitung.service.KundeService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/kunden")
@RequiredArgsConstructor
public class KundenController {

    private final KundeService kundeService;
    private final KundeMapper kundeMapper;

    @PostMapping()
    public ResponseEntity<?> addKunde(@RequestBody List<KundeDto> kundeDtos) {
        List<Kunde> kunden = kundeMapper.dtosToKunden(kundeDtos);

        try {
            kundeDtos = kundeMapper.kundenToDtos(kundeService.addKunden(kunden));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
        return ResponseEntity.ok(kundeDtos);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateKunde(@RequestBody KundeDto kundeDto) {
        Kunde kunde = kundeMapper.dtoToKunde(kundeDto);
        try {
            kundeDto = kundeMapper.kundeToDto(kundeService.updateKunde(kunde));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
        return ResponseEntity.ok(kundeDto);
    }


    @GetMapping()
    public ResponseEntity<List<KundeDto>> retrieveAllKundee() {
        // gets all Kunden from system and converts to dto
        List<KundeDto> kundeDtos = kundeMapper.kundenToDtos(kundeService.retrieveAllKunden());
        return ResponseEntity.ok(kundeDtos);
    }

    @GetMapping("/{id}")
    public ResponseEntity<KundeDto> retrieveKundeById(@PathVariable Long id) {
        // gets Kunde with given id and converts to dto
        KundeDto kundeDto = kundeMapper.kundeToDto(kundeService.retrieveKundeById(id));
        return ResponseEntity.ok(kundeDto);
    }

}
