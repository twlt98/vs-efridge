package de.thm.vs.efridge.auftragsverarbeitung.service;

import de.thm.vs.efridge.auftragsverarbeitung.data.entities.Produkt;
import de.thm.vs.efridge.auftragsverarbeitung.repository.ProduktRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProduktService {

    private final ProduktRepository produktRepository;

    public List<Produkt> addProdukte(List<Produkt> auftraege) {
        return produktRepository.saveAll(auftraege);
    }

    public Produkt updateProdukt(Produkt produkt) {
        if (produkt.getId() == null || !produktRepository.existsById(produkt.getId())) {
            // implement error
            return null;
        }
        return produktRepository.save(produkt);
    }

    public void deleteProduktById(Long id) {
        if (!produktRepository.existsById(id)) {
            // implement error
        }
        produktRepository.deleteById(id);
    }

    public List<Produkt> retrieveAllProdukte() {
        return produktRepository.findAll();
    }

    public Produkt retrieveProduktById(Long id) {
        return produktRepository.findById(id).orElseThrow();
    }

}
