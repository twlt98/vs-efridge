package de.thm.vs.efridge.auftragsverarbeitung.kafka.producer;

import de.thm.vs.efridge.auftragsverarbeitung.data.entities.Auftrag;
import de.thm.vs.efridge.auftragsverarbeitung.kafka.config.KafkaProperties;
import de.thm.vs.efridge.auftragsverarbeitung.kafka.events.KafkaAuftragEvent;
import de.thm.vs.efridge.auftragsverarbeitung.mapping.AuftragMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.UUID;


@Service
@RequiredArgsConstructor
public class AuftragEventProducerService {

    private final KafkaProperties kafkaProperties;
    private final AuftragMapper auftragMapper;
    private final KafkaTemplate<String, KafkaAuftragEvent> kafkaTemplate;

    public void publishAuftrag(Auftrag auftrag) {
        // creates uuid
        String uuid = UUID.randomUUID().toString();
        // sends auftrag to topic
        kafkaTemplate.send(kafkaProperties.topic().produktionssteuerungAuftrag(), uuid,
                KafkaAuftragEvent.builder()
                        .id(uuid)
                        .operationType("")
                        .auftrag(auftragMapper.auftragToDto(auftrag))
                        .build());
    }
}
