package de.thm.vs.efridge.auftragsverarbeitung.kafka.producer;

import de.thm.vs.efridge.auftragsverarbeitung.data.dto.SupportTicketDto;
import de.thm.vs.efridge.auftragsverarbeitung.kafka.config.KafkaProperties;
import de.thm.vs.efridge.auftragsverarbeitung.kafka.events.KafkaSupportEvent;
import lombok.RequiredArgsConstructor;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.UUID;


@Service
@RequiredArgsConstructor
public class SupportEventProducerService {

    private final KafkaProperties kafkaProperties;
    private final KafkaTemplate<String, KafkaSupportEvent> kafkaTemplate;

    public void publishSupportTicket(SupportTicketDto supportTicketDto) {
        String uuid = UUID.randomUUID().toString();
        kafkaTemplate.send(kafkaProperties.topic().support(), uuid,
                KafkaSupportEvent.builder()
                        .id(uuid)
                        .operationType("")
                        .supportTicket(supportTicketDto)
                        .build());
    }
}
