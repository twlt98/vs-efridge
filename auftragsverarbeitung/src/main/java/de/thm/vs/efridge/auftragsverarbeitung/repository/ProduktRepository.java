package de.thm.vs.efridge.auftragsverarbeitung.repository;

import de.thm.vs.efridge.auftragsverarbeitung.data.entities.Produkt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProduktRepository extends JpaRepository<Produkt, Long> {

}
