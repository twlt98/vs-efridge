package de.thm.vs.efridge.auftragsverarbeitung.repository;

import de.thm.vs.efridge.auftragsverarbeitung.data.entities.Auftrag;
import de.thm.vs.efridge.auftragsverarbeitung.kafka.repository.EventRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AuftragRepository extends EventRepository<Auftrag, Long> {

    Optional<Auftrag> findByKundeId(Long kundeId);

    List<Auftrag> findByStatus(String status);
}
