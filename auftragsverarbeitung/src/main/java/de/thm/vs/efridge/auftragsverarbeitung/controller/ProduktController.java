package de.thm.vs.efridge.auftragsverarbeitung.controller;

import de.thm.vs.efridge.auftragsverarbeitung.data.dto.ProduktDto;
import de.thm.vs.efridge.auftragsverarbeitung.data.entities.Produkt;
import de.thm.vs.efridge.auftragsverarbeitung.mapping.ProduktMapper;
import de.thm.vs.efridge.auftragsverarbeitung.service.ProduktService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/produkte")
@RequiredArgsConstructor
public class ProduktController {

    private final ProduktService produktService;
    private final ProduktMapper produktMapper;

    @PostMapping()
    public ResponseEntity<?> addProdukte(@RequestBody List<ProduktDto> produktDtos) {
        List<Produkt> auftraege = produktMapper.dtosToProdukte(produktDtos);
        try {
            produktDtos = produktMapper.produkteToDtos(produktService.addProdukte(auftraege));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
        return ResponseEntity.ok(produktDtos);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateProdukt(@RequestBody ProduktDto produktDto) {
        Produkt produkt = produktMapper.dtoToProdukt(produktDto);
        try {
            produktDto = produktMapper.produktToDto(produktService.updateProdukt(produkt));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
        return ResponseEntity.ok(produktDto);
    }

    @GetMapping()
    public ResponseEntity<List<ProduktDto>> retrieveAllProdukte() {
        // gets Produkt entities from system and converts to dtos
        List<ProduktDto> produktDtos = produktMapper.produkteToDtos(produktService.retrieveAllProdukte());
        return ResponseEntity.ok(produktDtos);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProduktDto> retrieveProduktById(@PathVariable Long id) {
        // gets Produkt with given id and converts to dto
        ProduktDto produktDto = produktMapper.produktToDto(produktService.retrieveProduktById(id));
        return ResponseEntity.ok(produktDto);
    }

}
