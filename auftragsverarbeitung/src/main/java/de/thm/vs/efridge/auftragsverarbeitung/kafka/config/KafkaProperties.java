package de.thm.vs.efridge.auftragsverarbeitung.kafka.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "spring.kafka")
public record KafkaProperties(String bootstrapServers, KafkaConsumerProperties consumer, KafkaTopics topic) {

    public record KafkaConsumerProperties(String groupId) {
    }

    public record KafkaTopics(String produktionssteuerungAuftrag, String auftragsverarbeitung,
                              String support, String auftragsverarbeitungSupport) {
    }
}
