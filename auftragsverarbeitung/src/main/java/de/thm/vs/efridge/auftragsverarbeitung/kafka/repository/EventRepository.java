package de.thm.vs.efridge.auftragsverarbeitung.kafka.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;
import java.util.List;

@NoRepositoryBean
public interface EventRepository<T, ID extends Serializable>
        extends JpaRepository<T, ID> {

    <S extends T> S saveAndPublish(S entity);
    <S extends T> List<S> saveAllAndPublish(Iterable<S> entities);
    T updateAndPublish(T entity);
}
