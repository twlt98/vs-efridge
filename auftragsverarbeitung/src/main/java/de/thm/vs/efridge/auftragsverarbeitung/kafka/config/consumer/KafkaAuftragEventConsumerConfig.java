package de.thm.vs.efridge.auftragsverarbeitung.kafka.config.consumer;

import de.thm.vs.efridge.auftragsverarbeitung.kafka.config.KafkaProperties;
import de.thm.vs.efridge.auftragsverarbeitung.kafka.events.KafkaAuftragEvent;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.mapping.DefaultJackson2JavaTypeMapper;
import org.springframework.kafka.support.mapping.Jackson2JavaTypeMapper;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.HashMap;
import java.util.Map;

@EnableKafka
@Configuration
public class KafkaAuftragEventConsumerConfig extends KafkaConsumerConfig<KafkaAuftragEvent>{


    public KafkaAuftragEventConsumerConfig(KafkaProperties kafkaProperties) {
        super(kafkaProperties);
    }


    @Override
    @Bean("auftragEventKafkaConsumerFactory")
    public ConsumerFactory<String, KafkaAuftragEvent> kafkaConsumerFactory() {
        JsonDeserializer<KafkaAuftragEvent> deserializer = new JsonDeserializer<>(KafkaAuftragEvent.class);
        DefaultJackson2JavaTypeMapper typeMapper = new DefaultJackson2JavaTypeMapper();
        // configure mapping
        Map<String, Class<?>> mappings = new HashMap<>();
        typeMapper.setTypePrecedence(Jackson2JavaTypeMapper.TypePrecedence.INFERRED);
        mappings.put("KafkaAuftragEvent", KafkaAuftragEvent.class);
        typeMapper.setIdClassMapping(mappings);
        deserializer.setTypeMapper(typeMapper);
        deserializer.addTrustedPackages("*");
        return new DefaultKafkaConsumerFactory<>(config(), new StringDeserializer(), deserializer);
    }


    @Override
    @Bean("auftragEventKafkaListenerContainerFactory")
    public ConcurrentKafkaListenerContainerFactory<String, KafkaAuftragEvent> kafkaListenerContainerFactory(
            @Qualifier("auftragEventKafkaConsumerFactory") ConsumerFactory<String, KafkaAuftragEvent> kafkaConsumerFactory) {
        ConcurrentKafkaListenerContainerFactory<String, KafkaAuftragEvent> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        // sets ConsumerFactory
        factory.setConsumerFactory(kafkaConsumerFactory());
        return factory;
    }
}
