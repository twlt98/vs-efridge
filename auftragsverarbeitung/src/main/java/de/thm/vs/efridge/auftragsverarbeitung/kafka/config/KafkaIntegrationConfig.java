package de.thm.vs.efridge.auftragsverarbeitung.kafka.config;

import de.thm.vs.efridge.auftragsverarbeitung.kafka.events.KafkaAuftragEvent;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.kafka.dsl.Kafka;
import org.springframework.kafka.core.ConsumerFactory;

@Configuration
@EnableIntegration
@RequiredArgsConstructor
public class KafkaIntegrationConfig {

    private final KafkaProperties kafkaProperties;

    private final ConsumerFactory<String, KafkaAuftragEvent> auftragConsumerFactory;

    @Bean
    public IntegrationFlow auftragFlow() {
        // creates and configures IntergrationFlow
        return IntegrationFlow
                .from(Kafka.messageDrivenChannelAdapter(auftragConsumerFactory,
                        kafkaProperties.topic().auftragsverarbeitung()))
                // messages are sent to auftragChannel
                .channel("auftragChannel")
                .get();
    }
}
