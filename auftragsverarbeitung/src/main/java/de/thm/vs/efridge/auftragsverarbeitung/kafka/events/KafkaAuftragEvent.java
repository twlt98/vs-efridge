package de.thm.vs.efridge.auftragsverarbeitung.kafka.events;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import de.thm.vs.efridge.auftragsverarbeitung.data.dto.AuftragDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonTypeName(value = "KafkaAuftragEvent")
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
public class KafkaAuftragEvent extends KafkaEvent {

    private AuftragDto auftrag;
}
