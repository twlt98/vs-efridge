package de.thm.vs.efridge.auftragsverarbeitung.kafka.config.producer;

import de.thm.vs.efridge.auftragsverarbeitung.kafka.config.KafkaProperties;
import de.thm.vs.efridge.auftragsverarbeitung.kafka.events.KafkaSupportEvent;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KafkaSupportEventProducerConfig extends KafkaProducerConfig<KafkaSupportEvent> {
    public KafkaSupportEventProducerConfig(KafkaProperties kafkaProperties) {
        super(kafkaProperties);
    }
}
