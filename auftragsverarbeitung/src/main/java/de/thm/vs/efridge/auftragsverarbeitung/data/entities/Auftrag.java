package de.thm.vs.efridge.auftragsverarbeitung.data.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name="auftrag")
public class Auftrag {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name="produktId")
    private Produkt produkt;

    @ManyToOne()
    @JoinColumn(name="kundeId")
    private Kunde kunde;

    @Column(name="status")
    private String status;

    @Column(name="anzahl")
    private int anzahl;
  }
