package de.thm.vs.efridge.auftragsverarbeitung.kafka.repository;

import de.thm.vs.efridge.auftragsverarbeitung.kafka.producer.ProducerCoordinator;
import jakarta.persistence.EntityManager;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.data.repository.core.RepositoryInformation;

public class EventRepositoryFactory extends JpaRepositoryFactory {

    private EntityManager entityManager;
    private ProducerCoordinator producerCoordinator;

    public EventRepositoryFactory(EntityManager entityManager, ProducerCoordinator producerCoordinator) {
        // creates EventRepositoryFactory and sets manager and coordinator
        super(entityManager);
        this.entityManager = entityManager;
        this.producerCoordinator = producerCoordinator;
    }


    @Override
    protected JpaRepositoryImplementation<?, ?> getTargetRepository(RepositoryInformation information, EntityManager entityManager) {
        return new EventRepositoryImpl(getEntityInformation(information.getDomainType()), entityManager, producerCoordinator);
    }
}
