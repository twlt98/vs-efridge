package de.thm.vs.efridge.auftragsverarbeitung.mapping;


import de.thm.vs.efridge.auftragsverarbeitung.data.dto.ProduktDto;
import de.thm.vs.efridge.auftragsverarbeitung.data.entities.Produkt;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ProduktMapper {

    ProduktDto produktToDto(Produkt produkt);

    List<ProduktDto> produkteToDtos(List<Produkt> produkte);

    Produkt dtoToProdukt(ProduktDto produktDto);

    List<Produkt> dtosToProdukte(List<ProduktDto> produktDtos);
}
