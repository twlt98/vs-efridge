package de.thm.vs.efridge.auftragsverarbeitung.data.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "produkt")
public class Produkt {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "beschreibung")
    private String beschreibung;

    @Column(name = "gewicht")
    private double gewicht;

    @Column(name = "dim")
    private String dim;

    @Column(name = "produktionszeit")
    private Long produktionszeit;
}
