package de.thm.vs.efridge.auftragsverarbeitung.kafka.repository;

import de.thm.vs.efridge.auftragsverarbeitung.kafka.producer.ProducerCoordinator;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class EventRepositoryImpl<T, ID extends Serializable> extends SimpleJpaRepository<T, ID>
        implements EventRepository<T, ID> {

    private ProducerCoordinator producerCoordinator;

    public EventRepositoryImpl(JpaEntityInformation<T, ID> entityInformation, EntityManager entityManager, ProducerCoordinator producerCoordinator) {
        super(entityInformation, entityManager);
        this.producerCoordinator = producerCoordinator;
    }

    @NotNull
    @Override
    @Transactional
    public <S extends T> S saveAndPublish(S entity) {
        // saves given entity
        entity = this.saveAndFlush(entity);
        // publish entity
        producerCoordinator.publishEvent(entity);
        return entity;
    }

    @NotNull
    @Override
    @Transactional()
    public <S extends T> List<S> saveAllAndPublish(Iterable<S> entities) {
        List<S> result = new ArrayList<>();
        for (S entity : entities) {
            // saves and publishes every entity in entities
            entity = this.saveAndFlush(entity);
            producerCoordinator.publishEvent(entity);
            result.add(this.saveAndFlush(entity));
        }
        return result;
    }

    @NotNull
    @Override
    @Transactional
    public T updateAndPublish(T entity) {
        producerCoordinator.publishEvent(entity);
        return this.saveAndFlush(entity);
    }
}
