package de.thm.vs.efridge.auftragsverarbeitung.kafka.producer;

import de.thm.vs.efridge.auftragsverarbeitung.data.entities.Auftrag;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ProducerCoordinator {

    private final AuftragEventProducerService auftragEventProducerService;

    public void publishEvent(Object obj) {
        // publishes object if it is an Auftrag
        if (obj instanceof Auftrag) {
            auftragEventProducerService.publishAuftrag((Auftrag) obj);
        }
    }
}
