package de.thm.vs.efridge.auftragsverarbeitung.controller;

import de.thm.vs.efridge.auftragsverarbeitung.data.dto.SupportTicketDto;
import de.thm.vs.efridge.auftragsverarbeitung.kafka.producer.SupportEventProducerService;
import de.thm.vs.efridge.auftragsverarbeitung.repository.KundeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/support")
@RequiredArgsConstructor
public class SupportTicketController {

    private final SupportEventProducerService supportEventProducerService;

    private final KundeRepository kundeRepository;

    @PostMapping()
    public ResponseEntity<?> addTickets(@RequestBody List<SupportTicketDto> supportTicketDtos) {
        for(SupportTicketDto s : supportTicketDtos) {
            if (kundeRepository.existsById(s.getKundeId())) {
                supportEventProducerService.publishSupportTicket(s);
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Kunde existiert nicht!");
            }
        }

        return ResponseEntity.ok(supportTicketDtos);
    }

}
