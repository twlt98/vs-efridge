package de.thm.vs.efridge.auftragsverarbeitung.mapping;


import de.thm.vs.efridge.auftragsverarbeitung.data.dto.AuftragDto;
import de.thm.vs.efridge.auftragsverarbeitung.data.entities.Auftrag;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, unmappedTargetPolicy = ReportingPolicy.IGNORE,
        uses = {ProduktMapper.class, KundeMapper.class})
public interface AuftragMapper {

    AuftragDto auftragToDto(Auftrag auftrag);

    List<AuftragDto> auftraegeToDtos(List<Auftrag> auftraege);

    Auftrag dtoToAuftrag(AuftragDto auftragDto);

    List<Auftrag> dtosToAuftraege(List<AuftragDto> auftragDtos);
}
