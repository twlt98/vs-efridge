package de.thm.vs.efridge.auftragsverarbeitung.kafka.events;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
public class KafkaEvent {
    protected String id;
    protected String operationType;
}
