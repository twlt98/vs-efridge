package de.thm.vs.efridge.auftragsverarbeitung;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
class AuftragsverarbeitungApplicationTests {

	@Test
	void contextLoads() {
	}

}
