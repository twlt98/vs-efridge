package de.thm.vs.efridge.produktionssteuerung;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
class ProduktionssteuerungApplicationTests {

	@Test
	void contextLoads() {
	}

}
