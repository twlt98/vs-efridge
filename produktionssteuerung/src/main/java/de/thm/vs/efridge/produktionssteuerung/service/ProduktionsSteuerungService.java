package de.thm.vs.efridge.produktionssteuerung.service;

import de.thm.vs.efridge.produktionssteuerung.data.dto.AuftragDto;
import de.thm.vs.efridge.produktionssteuerung.data.dto.WerkStatusDto;
import de.thm.vs.efridge.produktionssteuerung.kafka.events.KafkaAuftragEvent;
import de.thm.vs.efridge.produktionssteuerung.kafka.events.KafkaWerkStatusEvent;
import de.thm.vs.efridge.produktionssteuerung.kafka.producer.AuftragEventProducerService;
import de.thm.vs.efridge.produktionssteuerung.kafka.producer.PsStatusEventProducer;
import de.thm.vs.efridge.produktionssteuerung.kafka.producer.WerkAuftragEventProducerService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProduktionsSteuerungService {

    @Value("${self.name}")
    private String instanceName;

    private final WerkAuftragEventProducerService werkEventProducerService;

    private final AuftragEventProducerService auftragEventProducerService;

    private final PsStatusEventProducer psStatusEventProducerService;

    private Map<String, WerkStatusDto> werkStatusMap = new HashMap<>();

    private Map<String, List<AuftragDto>> werkAuftragMap = new HashMap<>();


    @ServiceActivator(inputChannel = "werkStatusChannel")
    public void handleWerkStatus(KafkaWerkStatusEvent kafkaWerkStatusEvent) {
        // gets werkstatus dto from werkstatusevent
        WerkStatusDto werkStatusDto = kafkaWerkStatusEvent.getWerkStatusDto();
        if (werkStatusDto.getProduktionssteuerung() != null) {
            if (werkStatusMap.containsKey(werkStatusDto.getWerkname())) {
                if (werkStatusDto.getProduktionssteuerung().equals(instanceName)) {
                    checkAuftragOffsetUpdate(werkStatusDto);
                } else {
                    redirectAuftraege(kafkaWerkStatusEvent.getWerkStatusDto().getWerkname());
                }
            } else if (werkStatusDto.getProduktionssteuerung().equals(instanceName)) {
                werkStatusMap.put(werkStatusDto.getWerkname(), werkStatusDto);
                System.out.println("Assigned Werk: " + kafkaWerkStatusEvent.getWerkStatusDto().getWerkname());
            }
        }
    }

    public void redirectAuftraege(String werkname) {
//        List<AuftragDto> toRedirect =  werkAuftragMap.get(werkname);

        werkStatusMap.remove(werkname);
        System.out.println("Removed Werk: " + werkname);

//        for (AuftragDto auftragDto : toRedirect) {
//            sendAuftragToWerk(auftragDto);
//        }
    }

    public void checkAuftragOffsetUpdate(WerkStatusDto werkStatusDto) {
        boolean shouldUpdate = true;
//        if(werkAuftragMap.containsKey(werkStatusDto.getWerkname())) {
//            List<AuftragDto> originalList = werkAuftragMap.get(werkStatusDto.getWerkname());
//            List<AuftragDto> copyList = new ArrayList<>(originalList);
//
//
//            for (AuftragDto auftragDto : copyList) {
//                if (!werkStatusDto.getPendingAuftraege().contains(auftragDto.getId())) {
//                    shouldUpdate = false;
//                    System.out.println("Reassigning Auftrag to: " + werkStatusDto.getWerkname());
//                    werkEventProducerService.sendAuftragToWerk(auftragDto, werkStatusDto.getWerkname() + "-auftrag");
//                }
//            }
//        }

        if (shouldUpdate) {
            werkStatusMap.put(werkStatusDto.getWerkname(), werkStatusDto);
            System.out.println("Updated WerkStatus: " + werkStatusDto.getWerkname());
        }
    }


    @ServiceActivator(inputChannel = "werkAuftragChannel")
    public void handleWerkAuftrag(KafkaAuftragEvent kafkaAuftragEvent) {
        System.out.println("Auftrag abgeschlossen: " + kafkaAuftragEvent.getAuftrag().toString());
        AuftragDto auftragDto = kafkaAuftragEvent.getAuftrag();

        for (Map.Entry<String, List<AuftragDto>> werkAuftraege : werkAuftragMap.entrySet()) {
            werkAuftraege.getValue().removeIf(value -> Objects.equals(value.getId(), auftragDto.getId()));
        }

        auftragDto.setStatus("DONE");
        auftragEventProducerService.publishAuftrag(auftragDto);
        psStatusEventProducerService.publishCurrentStatus(werkStatusMap, werkAuftragMap);
    }

    @ServiceActivator(inputChannel = "auftragChannel")
    public void handleAuftrag(KafkaAuftragEvent kafkaAuftragEvent) {
        System.out.println("Received Auftrag: " + kafkaAuftragEvent.getAuftrag().toString());

        AuftragDto auftragDto = kafkaAuftragEvent.getAuftrag();

        sendAuftragToWerk(auftragDto);
    }

    public void sendAuftragToWerk(AuftragDto auftragDto) {
        if (werkStatusMap.isEmpty()) {
            auftragDto.setStatus("RETRY");
            auftragEventProducerService.publishAuftrag(auftragDto);
            return;
        }

        // filter for werk with lowest auslastung
        WerkStatusDto werkStatusDto = werkStatusMap.values().stream().max(Comparator.comparingInt(
                w -> w.getKapa() - w.getPendingAuftraege().size()
        )).get();

        if (werkStatusDto.getKapa() - werkStatusDto.getPendingAuftraege().size() <= 0) {
            auftragDto.setStatus("RETRY");
            auftragEventProducerService.publishAuftrag(auftragDto);
            return;
        }

        // send auftrag to werk
        werkEventProducerService
                .sendAuftragToWerk(auftragDto,werkStatusDto.getWerkname() + "-auftrag");

        // add auftrag to pending list and update status
        List<AuftragDto> auftragDtos;
        auftragDto.setStatus("PENDING");

        auftragDtos = werkAuftragMap.containsKey(werkStatusDto.getWerkname()) ?
                werkAuftragMap.get(werkStatusDto.getWerkname()) : new ArrayList<>();

        auftragDtos.add(auftragDto);
        werkAuftragMap.put(werkStatusDto.getWerkname(), auftragDtos);

        // update auslastung in werkStatusMap
        List<Long> pending = werkStatusDto.getPendingAuftraege();
        pending.add(auftragDto.getId());
        werkStatusDto.setPendingAuftraege(pending);
        werkStatusMap.put(werkStatusDto.getWerkname(), werkStatusDto);

        // publish new state
        psStatusEventProducerService.publishCurrentStatus(werkStatusMap, werkAuftragMap);
    }
}
