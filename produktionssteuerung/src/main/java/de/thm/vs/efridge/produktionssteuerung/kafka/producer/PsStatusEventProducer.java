package de.thm.vs.efridge.produktionssteuerung.kafka.producer;

import de.thm.vs.efridge.produktionssteuerung.data.dto.AuftragDto;
import de.thm.vs.efridge.produktionssteuerung.data.dto.PsStatusDto;
import de.thm.vs.efridge.produktionssteuerung.data.dto.WerkStatusDto;
import de.thm.vs.efridge.produktionssteuerung.kafka.config.KafkaProperties;
import de.thm.vs.efridge.produktionssteuerung.kafka.events.KafkaPsStatusEvent;
import jakarta.annotation.PostConstruct;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Data
@Service
public class PsStatusEventProducer {

    @Value("${self.name}")
    private String instanceName;

    private final KafkaTemplate<String, KafkaPsStatusEvent> kafkaTemplate;

    private final KafkaProperties kafkaProperties;

    @PostConstruct
    public void publishInitalStatus() {
        PsStatusDto statusDto = new PsStatusDto();
        statusDto.setName(instanceName);
        statusDto.setWerkStatusMap(new HashMap<>());
        statusDto.setWerkAuftragMap(new HashMap<>());
        statusDto.setTimestamp(LocalDateTime.now());
        String uuid = UUID.randomUUID().toString();
        kafkaTemplate.send(kafkaProperties.topic().produktionssteuerungStatus(), uuid,
                KafkaPsStatusEvent.builder()
                        .id(instanceName).operationType("").produktionssteuerungStatusDto(statusDto).build());

        System.out.println("Published initial ProduktionssteuerungStatus for: " + instanceName);
    }

    public void publishCurrentStatus(Map<String, WerkStatusDto> werkStatusMap, Map<String, List<AuftragDto>> werkAuftragMap) {
        PsStatusDto statusDto = new PsStatusDto();
        // sets status properties
        statusDto.setName(instanceName);
        statusDto.setWerkStatusMap(werkStatusMap);
        statusDto.setWerkAuftragMap(werkAuftragMap);
        statusDto.setTimestamp(LocalDateTime.now());
        String uuid = UUID.randomUUID().toString();
        // publishes current status through template
        kafkaTemplate.send(kafkaProperties.topic().produktionssteuerungStatus(), uuid,
                KafkaPsStatusEvent.builder()
                        .id(uuid).operationType("").produktionssteuerungStatusDto(statusDto).build());
    }
}
