package de.thm.vs.efridge.produktionssteuerung.kafka.events;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import de.thm.vs.efridge.produktionssteuerung.data.dto.WerkStatusDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonTypeName(value = "KafkaWerkStatusEvent")
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
public class KafkaWerkStatusEvent extends KafkaEvent {

    private WerkStatusDto werkStatusDto;
}
