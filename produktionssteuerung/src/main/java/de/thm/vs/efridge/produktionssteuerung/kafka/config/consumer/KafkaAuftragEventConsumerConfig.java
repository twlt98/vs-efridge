package de.thm.vs.efridge.produktionssteuerung.kafka.config.consumer;

import de.thm.vs.efridge.produktionssteuerung.kafka.config.KafkaProperties;
import de.thm.vs.efridge.produktionssteuerung.kafka.events.KafkaAuftragEvent;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.kafka.support.mapping.DefaultJackson2JavaTypeMapper;
import org.springframework.kafka.support.mapping.Jackson2JavaTypeMapper;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.HashMap;
import java.util.Map;

@EnableKafka
@Configuration
public class KafkaAuftragEventConsumerConfig extends KafkaConsumerConfig<KafkaAuftragEvent> {

    public KafkaAuftragEventConsumerConfig(KafkaProperties kafkaProperties) {
        super(kafkaProperties);
    }

    @Override
    @Bean(name = "kafkaAuftragEventConsumerFactory")
    public ConsumerFactory<String, KafkaAuftragEvent> kafkaConsumerFactory() {
        JsonDeserializer<KafkaAuftragEvent> deserializer = new JsonDeserializer<>(KafkaAuftragEvent.class);
        DefaultJackson2JavaTypeMapper typeMapper = new DefaultJackson2JavaTypeMapper();
        // creates hashmap to store configuration for ConsumerFactory
        Map<String, Class<?>> mappings = new HashMap<>();
        typeMapper.setTypePrecedence(Jackson2JavaTypeMapper.TypePrecedence.INFERRED);
        mappings.put("KafkaAuftragEvent", KafkaAuftragEvent.class);
        typeMapper.setIdClassMapping(mappings);
        deserializer.setTypeMapper(typeMapper);
        deserializer.addTrustedPackages("*");
        return new DefaultKafkaConsumerFactory<>(config(), new StringDeserializer(), deserializer);
    }

    @Override
    @Bean(name = "kafkaAuftragEventListenerContainerFactory")
    public ConcurrentKafkaListenerContainerFactory<String, KafkaAuftragEvent> kafkaListenerContainerFactory(
           @Autowired
           @Qualifier(value = "kafkaAuftragEventConsumerFactory")
           ConsumerFactory<String, KafkaAuftragEvent> kafkaConsumerFactory
    ) {
        // creates ListenerContainerFactory and sets ConsumerFactory
        ConcurrentKafkaListenerContainerFactory<String, KafkaAuftragEvent> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(kafkaConsumerFactory);
        return factory;
    }
}
