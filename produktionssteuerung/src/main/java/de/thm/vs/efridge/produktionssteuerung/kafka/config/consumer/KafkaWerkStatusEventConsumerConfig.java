package de.thm.vs.efridge.produktionssteuerung.kafka.config.consumer;

import de.thm.vs.efridge.produktionssteuerung.kafka.config.KafkaProperties;
import de.thm.vs.efridge.produktionssteuerung.kafka.events.KafkaAuftragEvent;
import de.thm.vs.efridge.produktionssteuerung.kafka.events.KafkaWerkStatusEvent;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.kafka.support.mapping.DefaultJackson2JavaTypeMapper;
import org.springframework.kafka.support.mapping.Jackson2JavaTypeMapper;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.HashMap;
import java.util.Map;

@EnableKafka
@Configuration
public class KafkaWerkStatusEventConsumerConfig extends KafkaConsumerConfig<KafkaWerkStatusEvent> {

    public KafkaWerkStatusEventConsumerConfig(KafkaProperties kafkaProperties) {
        super(kafkaProperties);
    }

    @Override
    @Bean(name = "kafkaWerkStatusEventConsumerFactory")
    public ConsumerFactory<String, KafkaWerkStatusEvent> kafkaConsumerFactory() {
        JsonDeserializer<KafkaWerkStatusEvent> deserializer = new JsonDeserializer<>(KafkaWerkStatusEvent.class);
        DefaultJackson2JavaTypeMapper typeMapper = new DefaultJackson2JavaTypeMapper();
        // hashmap to store configuration properties
        Map<String, Class<?>> mappings = new HashMap<>();
        typeMapper.setTypePrecedence(Jackson2JavaTypeMapper.TypePrecedence.INFERRED);
        mappings.put("KafkaWerkStatusEvent", KafkaWerkStatusEvent.class);
        typeMapper.setIdClassMapping(mappings);
        deserializer.setTypeMapper(typeMapper);
        deserializer.addTrustedPackages("*");
        return new DefaultKafkaConsumerFactory<>(config(), new StringDeserializer(), deserializer);
    }

    @Override
    @Bean(name = "kafkaWerkStatusEventListenerContainerFactory")
    public ConcurrentKafkaListenerContainerFactory<String, KafkaWerkStatusEvent> kafkaListenerContainerFactory(
           @Autowired
           @Qualifier(value = "kafkaWerkStatusEventConsumerFactory")
           ConsumerFactory<String, KafkaWerkStatusEvent> kafkaConsumerFactory
    ) {
        ConcurrentKafkaListenerContainerFactory<String, KafkaWerkStatusEvent> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(kafkaConsumerFactory);
        return factory;
    }

    @Bean
    public Consumer<String, KafkaWerkStatusEvent> createWkStatusKafkaConsumer(
            @Autowired
            @Qualifier(value = "kafkaWerkStatusEventConsumerFactory")
            ConsumerFactory<String, KafkaWerkStatusEvent> kafkaConsumerFactory
    ) {
        return kafkaConsumerFactory.createConsumer();
    }
}
