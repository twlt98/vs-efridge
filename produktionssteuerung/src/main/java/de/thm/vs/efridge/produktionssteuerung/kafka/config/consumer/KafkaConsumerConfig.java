package de.thm.vs.efridge.produktionssteuerung.kafka.config.consumer;

import de.thm.vs.efridge.produktionssteuerung.kafka.config.KafkaProperties;
import de.thm.vs.efridge.produktionssteuerung.kafka.events.KafkaAuftragEvent;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.HashMap;
import java.util.Map;

@Configuration
@RequiredArgsConstructor
public abstract class KafkaConsumerConfig<T> {

    protected final KafkaProperties kafkaProperties;

    public Map<String, Object> config() {
        // hashmap for storing config
        Map<String, Object> conf = new HashMap<>();
        conf.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaProperties.bootstrapServers());
        conf.put(ConsumerConfig.GROUP_ID_CONFIG, kafkaProperties.consumer().groupId());
        conf.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        conf.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
        return conf;
    }

    public abstract ConsumerFactory<String, T> kafkaConsumerFactory();

    public abstract ConcurrentKafkaListenerContainerFactory<String, T> kafkaListenerContainerFactory(
            @Autowired ConsumerFactory<String, T> kafkaConsumerFactory
    );
}
