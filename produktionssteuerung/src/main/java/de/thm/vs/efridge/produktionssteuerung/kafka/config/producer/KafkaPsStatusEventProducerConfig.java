package de.thm.vs.efridge.produktionssteuerung.kafka.config.producer;

import de.thm.vs.efridge.produktionssteuerung.kafka.config.KafkaProperties;
import de.thm.vs.efridge.produktionssteuerung.kafka.events.KafkaPsStatusEvent;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KafkaPsStatusEventProducerConfig
        extends KafkaProducerConfig<KafkaPsStatusEvent> {

    public KafkaPsStatusEventProducerConfig(KafkaProperties kafkaProperties) {
        super(kafkaProperties);
    }
}
