package de.thm.vs.efridge.produktionssteuerung;

import de.thm.vs.efridge.produktionssteuerung.kafka.config.KafkaProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(value = KafkaProperties.class)
public class ProduktionssteuerungApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProduktionssteuerungApplication.class, args);
    }

}
