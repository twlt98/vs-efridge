package de.thm.vs.efridge.produktionssteuerung.data.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProduktDto {

    private Long id;

    private String name;

    private String beschreibung;

    private double gewicht;

    private String dim;

    private Long produktionszeit;
}
