package de.thm.vs.efridge.produktionssteuerung.kafka.config;

import de.thm.vs.efridge.produktionssteuerung.kafka.events.KafkaAuftragEvent;
import de.thm.vs.efridge.produktionssteuerung.kafka.events.KafkaWerkStatusEvent;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.kafka.dsl.Kafka;
import org.springframework.kafka.core.ConsumerFactory;

@Configuration
@EnableIntegration
public class KafkaIntegrationConfig {

    private final KafkaProperties kafkaProperties;
    private final String psAuftragTopic;
    private final ConsumerFactory<String, KafkaWerkStatusEvent> werkStatusConsumerFactory;
    private final ConsumerFactory<String, KafkaAuftragEvent> auftragConsumerFactory;

    public KafkaIntegrationConfig(
            KafkaProperties kafkaProperties,
            @Qualifier(value = "kafkaWerkStatusEventConsumerFactory")
            ConsumerFactory<String, KafkaWerkStatusEvent> werkStatusConsumerFactory,
            @Qualifier(value = "kafkaAuftragEventConsumerFactory")
            ConsumerFactory<String, KafkaAuftragEvent> auftragConsumerFactory,
            @Value("${self.name}")
            String instance) {
        // sets properties
        this.kafkaProperties = kafkaProperties;
        this.werkStatusConsumerFactory = werkStatusConsumerFactory;
        this.auftragConsumerFactory = auftragConsumerFactory;
        psAuftragTopic =  instance + "-auftrag";
    }

    @Bean
    public IntegrationFlow werkStatusFlow() {
        // creates IntegrationFlow and sets channel for werkstatus
        return IntegrationFlow
                .from(Kafka.messageDrivenChannelAdapter(werkStatusConsumerFactory,
                        kafkaProperties.topic().werkstatus()))
                .channel("werkStatusChannel")
                .get();
    }

    @Bean
    public IntegrationFlow auftragFlow() {
        // creates IntegrationFlow and sets channel for auftraege
        return IntegrationFlow
                .from(Kafka.messageDrivenChannelAdapter(auftragConsumerFactory,
                        kafkaProperties.topic().produktionssteuerungAuftrag()))
                .channel("auftragChannel")
                .get();
    }

    @Bean
    public IntegrationFlow werkAuftragFlow() {
        // creates IntegrationFlow and sets channel for werkauftrag
        return IntegrationFlow
                .from(Kafka.messageDrivenChannelAdapter(auftragConsumerFactory, psAuftragTopic
                        ))
                .channel("werkAuftragChannel")
                .get();
    }
}
