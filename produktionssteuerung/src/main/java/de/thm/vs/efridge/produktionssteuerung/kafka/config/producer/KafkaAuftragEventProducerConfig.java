package de.thm.vs.efridge.produktionssteuerung.kafka.config.producer;

import de.thm.vs.efridge.produktionssteuerung.kafka.config.KafkaProperties;
import de.thm.vs.efridge.produktionssteuerung.kafka.events.KafkaAuftragEvent;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KafkaAuftragEventProducerConfig extends KafkaProducerConfig<KafkaAuftragEvent> {

    public KafkaAuftragEventProducerConfig(KafkaProperties kafkaProperties) {
        super(kafkaProperties);
    }
}
