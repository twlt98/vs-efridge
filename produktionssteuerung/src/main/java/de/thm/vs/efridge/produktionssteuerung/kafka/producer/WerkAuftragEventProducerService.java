package de.thm.vs.efridge.produktionssteuerung.kafka.producer;

import de.thm.vs.efridge.produktionssteuerung.data.dto.AuftragDto;
import de.thm.vs.efridge.produktionssteuerung.kafka.events.KafkaAuftragEvent;
import lombok.Data;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Data
@Service
public class WerkAuftragEventProducerService {

    private final KafkaTemplate<String, KafkaAuftragEvent> kafkaTemplate;

    public void sendAuftragToWerk(AuftragDto auftragDto, String werkTopic) {
        // creates unique uuid
        String uuid = UUID.randomUUID().toString();
        // publish Auftrag to werkTopic
        CompletableFuture<SendResult<String, KafkaAuftragEvent>> future =
                kafkaTemplate.send(werkTopic, KafkaAuftragEvent.builder()
                        .id(uuid)
                        .operationType("")
                        .auftrag(auftragDto)
                        .build());
    }
}
