package de.thm.vs.efridge.produktionssteuerung.kafka.producer;

import de.thm.vs.efridge.produktionssteuerung.data.dto.AuftragDto;
import de.thm.vs.efridge.produktionssteuerung.kafka.config.KafkaProperties;
import de.thm.vs.efridge.produktionssteuerung.kafka.events.KafkaAuftragEvent;
import lombok.RequiredArgsConstructor;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.UUID;


@Service
@RequiredArgsConstructor
public class AuftragEventProducerService {

    private final KafkaProperties kafkaProperties;
    private final KafkaTemplate<String, KafkaAuftragEvent> kafkaTemplate;

    public void publishAuftrag(AuftragDto auftragDto) {
        // create unique uuid
        String uuid = UUID.randomUUID().toString();
        // publishes auftrag with uuid
        kafkaTemplate.send(kafkaProperties.topic().auftragsverarbeitung(), uuid,
                KafkaAuftragEvent.builder()
                        .id(uuid)
                        .operationType("")
                        .auftrag(auftragDto)
                        .build());
    }
}
