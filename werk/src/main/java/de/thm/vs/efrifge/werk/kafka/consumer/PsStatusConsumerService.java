package de.thm.vs.efrifge.werk.kafka.consumer;

import de.thm.vs.efrifge.werk.kafka.config.KafkaProperties;
import de.thm.vs.efrifge.werk.kafka.events.KafkaPsStatusEvent;
import de.thm.vs.efrifge.werk.service.ProduktionsService;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.Collections;

@Service
@RequiredArgsConstructor
public class PsStatusConsumerService {

    private final ProduktionsService produktionsService;

    private final KafkaProperties kafkaProperties;

    private final Consumer<String, KafkaPsStatusEvent> psStatusConsumer;

    @PostConstruct
    public void init() {
        Thread initialAssignmentThread = new Thread(this::initalPollFromPsStatusTopic);
        initialAssignmentThread.start();
    }

    public void initalPollFromPsStatusTopic() {
        try (psStatusConsumer) {
            psStatusConsumer.subscribe(Collections.singletonList(kafkaProperties.topic().produktionssteuerungStatus()));

            // Warten, bis die Partitionen zugewiesen sind
            while (psStatusConsumer.assignment().isEmpty()) {
                psStatusConsumer.poll(Duration.ofMillis(100));
            }

            // Setzen Sie den Offset für jede Partition manuell
            psStatusConsumer.assignment().forEach(topicPartition -> psStatusConsumer.seek(topicPartition, 0));

            boolean keepReading = true;
            while (keepReading) {
                ConsumerRecords<String, KafkaPsStatusEvent> records
                        = psStatusConsumer.poll(Duration.ofMillis(100));

                if (records.isEmpty()) {
                    keepReading = false;
                }
            }
        }

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            return;
        }

        produktionsService.setAssignedProduktionssteuerung();
    }
}
