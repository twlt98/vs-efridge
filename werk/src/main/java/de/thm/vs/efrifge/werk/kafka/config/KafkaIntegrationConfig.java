package de.thm.vs.efrifge.werk.kafka.config;

import de.thm.vs.efrifge.werk.kafka.events.KafkaAuftragEvent;
import de.thm.vs.efrifge.werk.kafka.events.KafkaBestellungWerkEvent;
import de.thm.vs.efrifge.werk.kafka.events.KafkaPsStatusEvent;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.kafka.dsl.Kafka;
import org.springframework.kafka.core.ConsumerFactory;

@Configuration
@EnableIntegration
public class KafkaIntegrationConfig {

    private final KafkaProperties kafkaProperties;
    private final ConsumerFactory<String, KafkaPsStatusEvent> psStatusConsumerFactory;
    private final ConsumerFactory<String, KafkaAuftragEvent> auftragConsumerFactory;
    private final ConsumerFactory<String, KafkaBestellungWerkEvent> bestellungWerkEventConsumerFactory;
    private final String werkAuftragTopic;
    private final String werkBestellungTopic;

    public KafkaIntegrationConfig(
            KafkaProperties kafkaProperties,
            @Qualifier(value = "kafkaPsStatusEventConsumerFactory")
            ConsumerFactory<String, KafkaPsStatusEvent> psStatusConsumerFactory,
            @Qualifier(value = "kafkaAuftragEventConsumerFactory")
            ConsumerFactory<String, KafkaAuftragEvent> auftragConsumerFactory,
            @Qualifier(value = "kafkaBestellungWerkEventConsumerFactory")
            ConsumerFactory<String, KafkaBestellungWerkEvent> bestellungWerkEventConsumerFactory,
            @Value("${self.name}") String instance) {
        this.kafkaProperties = kafkaProperties;
        this.psStatusConsumerFactory = psStatusConsumerFactory;
        this.auftragConsumerFactory = auftragConsumerFactory;
        this.bestellungWerkEventConsumerFactory = bestellungWerkEventConsumerFactory;
        werkAuftragTopic = instance + "-auftrag";
        werkBestellungTopic = instance + "-bestellung";
    }

    @Bean
    public IntegrationFlow psStatusFlow() {
        return IntegrationFlow
                .from(Kafka.messageDrivenChannelAdapter(psStatusConsumerFactory,
                        kafkaProperties.topic().produktionssteuerungStatus()))
                .channel("psStatusChannel")
                .get();
    }

    @Bean
    public IntegrationFlow psAuftragFlow() {
        return IntegrationFlow
                .from(Kafka.messageDrivenChannelAdapter(auftragConsumerFactory,
                        werkAuftragTopic))
                .channel("psAuftragChannel")
                .get();
    }

    @Bean
    public IntegrationFlow bestellungWerkFlow() {
        return IntegrationFlow
                .from(Kafka.messageDrivenChannelAdapter(bestellungWerkEventConsumerFactory,
                        werkBestellungTopic))
                .channel("bestellungWerkChannel")
                .get();
    }
}
