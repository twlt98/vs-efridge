package de.thm.vs.efrifge.werk.kafka.config.producer;

import de.thm.vs.efrifge.werk.kafka.config.KafkaProperties;
import de.thm.vs.efrifge.werk.kafka.events.KafkaWerkStatusEvent;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KafkaWerkStatusEventProducerConfig
        extends KafkaProducerConfig<KafkaWerkStatusEvent> {

    public KafkaWerkStatusEventProducerConfig(KafkaProperties kafkaProperties) {
        super(kafkaProperties);
    }
}
