package de.thm.vs.efrifge.werk.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PsStatusDto {

    private String name;

    private Map<String, WerkStatusDto> werkStatusMap;
}
