package de.thm.vs.efrifge.werk.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BestellungWerkDto {
    private Long auftragId;
    private String status;
    private String werk;
    private Long produktId;
    private int anzahl;
}
