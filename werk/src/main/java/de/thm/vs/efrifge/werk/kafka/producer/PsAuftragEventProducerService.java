package de.thm.vs.efrifge.werk.kafka.producer;

import de.thm.vs.efrifge.werk.dto.AuftragDto;
import de.thm.vs.efrifge.werk.kafka.events.KafkaAuftragEvent;
import lombok.Data;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Data
@Service
public class PsAuftragEventProducerService {

    private final KafkaTemplate<String, KafkaAuftragEvent> kafkaTemplate;

    public void sendAuftragToProduktionssteuerung(AuftragDto auftragDto, String psTopic) {
        String uuid = UUID.randomUUID().toString();
        kafkaTemplate.send(psTopic, KafkaAuftragEvent.builder()
                .id(uuid)
                .operationType("")
                .auftrag(auftragDto)
                .build());

    }
}
