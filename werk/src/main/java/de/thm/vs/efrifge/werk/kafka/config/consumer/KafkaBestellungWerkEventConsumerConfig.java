package de.thm.vs.efrifge.werk.kafka.config.consumer;

import de.thm.vs.efrifge.werk.kafka.config.KafkaProperties;
import de.thm.vs.efrifge.werk.kafka.events.KafkaBestellungWerkEvent;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.mapping.DefaultJackson2JavaTypeMapper;
import org.springframework.kafka.support.mapping.Jackson2JavaTypeMapper;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.HashMap;
import java.util.Map;

@EnableKafka
@Configuration
public class KafkaBestellungWerkEventConsumerConfig extends KafkaConsumerConfig<KafkaBestellungWerkEvent> {

    public KafkaBestellungWerkEventConsumerConfig(KafkaProperties kafkaProperties) {
        super(kafkaProperties);
    }

    @Override
    @Bean(name = "kafkaBestellungWerkEventConsumerFactory")
    public ConsumerFactory<String, KafkaBestellungWerkEvent> kafkaConsumerFactory() {
        JsonDeserializer<KafkaBestellungWerkEvent> deserializer = new JsonDeserializer<>(KafkaBestellungWerkEvent.class);
        DefaultJackson2JavaTypeMapper typeMapper = new DefaultJackson2JavaTypeMapper();
        Map<String, Class<?>> mappings = new HashMap<>();
        typeMapper.setTypePrecedence(Jackson2JavaTypeMapper.TypePrecedence.INFERRED);
        mappings.put("KafkaBestellungWerkEvent", KafkaBestellungWerkEvent.class);
        typeMapper.setIdClassMapping(mappings);
        deserializer.setTypeMapper(typeMapper);
        deserializer.addTrustedPackages("*");
        return new DefaultKafkaConsumerFactory<>(config(), new StringDeserializer(), deserializer);
    }

    @Override
    @Bean(name = "kafkaBestellungWerkEventListenerContainerFactory")
    public ConcurrentKafkaListenerContainerFactory<String, KafkaBestellungWerkEvent> kafkaListenerContainerFactory(
           @Autowired
           @Qualifier(value = "kafkaBestellungWerkEventConsumerFactory")
           ConsumerFactory<String, KafkaBestellungWerkEvent> kafkaConsumerFactory
    ) {
        ConcurrentKafkaListenerContainerFactory<String, KafkaBestellungWerkEvent> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(kafkaConsumerFactory);

        return factory;
    }

    @Bean
    public Consumer<String, KafkaBestellungWerkEvent> createBestellungWerkKafkaConsumer(
            @Autowired
            @Qualifier(value = "kafkaBestellungWerkEventConsumerFactory")
            ConsumerFactory<String, KafkaBestellungWerkEvent> kafkaConsumerFactory
    ) {
        return kafkaConsumerFactory.createConsumer();
    }
}
