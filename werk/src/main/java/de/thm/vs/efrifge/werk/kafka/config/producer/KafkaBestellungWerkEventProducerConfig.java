package de.thm.vs.efrifge.werk.kafka.config.producer;

import de.thm.vs.efrifge.werk.kafka.config.KafkaProperties;
import de.thm.vs.efrifge.werk.kafka.events.KafkaBestellungWerkEvent;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KafkaBestellungWerkEventProducerConfig
        extends KafkaProducerConfig<KafkaBestellungWerkEvent> {

    public KafkaBestellungWerkEventProducerConfig(KafkaProperties kafkaProperties) {
        super(kafkaProperties);
    }
}
