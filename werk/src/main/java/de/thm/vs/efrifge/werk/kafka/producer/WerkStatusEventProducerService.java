package de.thm.vs.efrifge.werk.kafka.producer;

import de.thm.vs.efrifge.werk.dto.WerkStatusDto;
import de.thm.vs.efrifge.werk.kafka.config.KafkaProperties;
import de.thm.vs.efrifge.werk.kafka.events.KafkaWerkStatusEvent;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@Service
public class WerkStatusEventProducerService {

    @Value("${self.name}")
    private String instanceName;

    @Value("${self.standort}")
    private String standort;

    @Value("${self.kapa}")
    private int kapa;

    private final KafkaTemplate<String, KafkaWerkStatusEvent> kafkaTemplate;

    private final KafkaProperties kafkaProperties;

    @PostConstruct
    private void publishInitialStatus() {
        WerkStatusDto statusDto = new WerkStatusDto();
        statusDto.setWerkname(instanceName);
        statusDto.setKapa(kapa);
        statusDto.setPendingAuftraege(new ArrayList<>());
        statusDto.setProduktionssteuerung(null);
        statusDto.setStandort(standort);
        String uuid = UUID.randomUUID().toString();
        kafkaTemplate.send(kafkaProperties.topic().werkstatus(), uuid,
                KafkaWerkStatusEvent.builder()
                        .id(uuid).operationType("").werkStatusDto(statusDto).build());

        System.out.println("Published initial Werkstatus!");
    }

    public void publishCurrentStatus(String assignedPs, List<Long> pending) {
        WerkStatusDto statusDto = new WerkStatusDto();
        statusDto.setWerkname(instanceName);
        statusDto.setKapa(kapa);
        statusDto.setPendingAuftraege(pending);
        statusDto.setProduktionssteuerung(assignedPs);
        statusDto.setStandort(standort);
        String uuid = UUID.randomUUID().toString();
        kafkaTemplate.send(kafkaProperties.topic().werkstatus(), uuid,
                KafkaWerkStatusEvent.builder()
                        .id(uuid).operationType("").werkStatusDto(statusDto).build());
    }
}
