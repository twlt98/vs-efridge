package de.thm.vs.efrifge.werk.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class WerkStatusDto {

    private int kapa;

    private String werkname;

    private String standort;

    private String produktionssteuerung;

    private List<Long> pendingAuftraege = new ArrayList<>();
}
