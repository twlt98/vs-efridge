package de.thm.vs.efrifge.werk.kafka.events;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import de.thm.vs.efrifge.werk.dto.WerkStatusDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonTypeName(value = "KafkaWerkStatusEvent")
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
public class KafkaWerkStatusEvent extends KafkaEvent {

    private WerkStatusDto werkStatusDto;
}
