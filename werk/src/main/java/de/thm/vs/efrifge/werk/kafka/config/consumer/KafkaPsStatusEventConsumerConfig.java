package de.thm.vs.efrifge.werk.kafka.config.consumer;

import de.thm.vs.efrifge.werk.kafka.config.KafkaProperties;
import de.thm.vs.efrifge.werk.kafka.events.KafkaPsStatusEvent;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.mapping.DefaultJackson2JavaTypeMapper;
import org.springframework.kafka.support.mapping.Jackson2JavaTypeMapper;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.HashMap;
import java.util.Map;

@EnableKafka
@Configuration
public class KafkaPsStatusEventConsumerConfig extends KafkaConsumerConfig<KafkaPsStatusEvent> {

    public KafkaPsStatusEventConsumerConfig(KafkaProperties kafkaProperties) {
        super(kafkaProperties);
    }

    @Override
    @Bean(name = "kafkaPsStatusEventConsumerFactory")
    public ConsumerFactory<String, KafkaPsStatusEvent> kafkaConsumerFactory() {
        JsonDeserializer<KafkaPsStatusEvent> deserializer = new JsonDeserializer<>(KafkaPsStatusEvent.class);
        DefaultJackson2JavaTypeMapper typeMapper = new DefaultJackson2JavaTypeMapper();
        Map<String, Class<?>> mappings = new HashMap<>();
        typeMapper.setTypePrecedence(Jackson2JavaTypeMapper.TypePrecedence.INFERRED);
        mappings.put("KafkaProduktionssteuerungStatusEvent", KafkaPsStatusEvent.class);
        typeMapper.setIdClassMapping(mappings);
        deserializer.setTypeMapper(typeMapper);
        deserializer.addTrustedPackages("*");
        return new DefaultKafkaConsumerFactory<>(config(), new StringDeserializer(), deserializer);
    }

    @Override
    @Bean(name = "kafkaPsStatusEventListenerContainerFactory")
    public ConcurrentKafkaListenerContainerFactory<String, KafkaPsStatusEvent> kafkaListenerContainerFactory(
           @Autowired
           @Qualifier(value = "kafkaPsStatusEventConsumerFactory")
           ConsumerFactory<String, KafkaPsStatusEvent> kafkaConsumerFactory
    ) {
        ConcurrentKafkaListenerContainerFactory<String, KafkaPsStatusEvent> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(kafkaConsumerFactory);

        return factory;
    }

    @Bean
    public Consumer<String, KafkaPsStatusEvent> createKafkaConsumer(
            @Autowired
            @Qualifier(value = "kafkaPsStatusEventConsumerFactory")
            ConsumerFactory<String, KafkaPsStatusEvent> kafkaConsumerFactory
    ) {
        return kafkaConsumerFactory.createConsumer();
    }
}
