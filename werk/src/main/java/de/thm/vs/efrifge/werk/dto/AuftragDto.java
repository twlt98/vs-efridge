package de.thm.vs.efrifge.werk.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuftragDto {

    private Long id;

    private ProduktDto produkt;

    private KundeDto kunde;

    private String status;

    private int anzahl;
}
