package de.thm.vs.efrifge.werk.kafka.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "spring.kafka")
public record KafkaProperties(String bootstrapServers, KafkaTopics topic, KafkaConsumerProperties consumer) {

    public record KafkaConsumerProperties(String groupId) {
    }

    public record KafkaTopics(String produktionssteuerungStatus, String werkstatus, String lieferantenmanagement) {
    }
}
