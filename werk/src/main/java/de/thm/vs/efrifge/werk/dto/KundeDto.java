package de.thm.vs.efrifge.werk.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class KundeDto {

    private Long id;

    private String name;

    private String adresse;

    private String email;

    private String passwort;
}
