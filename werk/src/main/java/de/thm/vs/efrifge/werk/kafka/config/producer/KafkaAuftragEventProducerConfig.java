package de.thm.vs.efrifge.werk.kafka.config.producer;

import de.thm.vs.efrifge.werk.kafka.config.KafkaProperties;
import de.thm.vs.efrifge.werk.kafka.events.KafkaAuftragEvent;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KafkaAuftragEventProducerConfig extends KafkaProducerConfig<KafkaAuftragEvent> {

    public KafkaAuftragEventProducerConfig(KafkaProperties kafkaProperties) {
        super(kafkaProperties);
    }
}
