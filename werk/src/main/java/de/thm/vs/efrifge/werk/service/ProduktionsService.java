package de.thm.vs.efrifge.werk.service;


import de.thm.vs.efrifge.werk.dto.AuftragDto;
import de.thm.vs.efrifge.werk.dto.BestellungWerkDto;
import de.thm.vs.efrifge.werk.dto.PsStatusDto;
import de.thm.vs.efrifge.werk.kafka.events.KafkaAuftragEvent;
import de.thm.vs.efrifge.werk.kafka.events.KafkaBestellungWerkEvent;
import de.thm.vs.efrifge.werk.kafka.events.KafkaPsStatusEvent;
import de.thm.vs.efrifge.werk.kafka.producer.BestellungWerkEventProducerService;
import de.thm.vs.efrifge.werk.kafka.producer.PsAuftragEventProducerService;
import de.thm.vs.efrifge.werk.kafka.producer.WerkStatusEventProducerService;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class ProduktionsService {

    private final Object lock = new Object();

    private final Map<Long, AuftragDto> pendingWerkAuftraege = new HashMap<>();

    private final Map<Long, BestellungWerkDto> pendingBestellungen = new HashMap<>();

    private final Map<Long, BestellungWerkDto> receivedBestellungen = new HashMap<>();

    private final Map<String, PsStatusDto> psStatusMap = new HashMap<>();

    private final PsAuftragEventProducerService psAuftragEventProducerService;

    private final WerkStatusEventProducerService werkStatusEventProducerService;

    private final BestellungWerkEventProducerService bestellungWerkEventProducerService;

    private boolean enablePostStartupAssigment = false;

    private String assignedProduktionssteuerung = null;

    @Value("${self.name}")
    private String instanceName;

    @PostConstruct
    public void init() {
        Thread auftragsVerarbeitungThread = new Thread(this::verarbeiteAuftraege);
        auftragsVerarbeitungThread.start();
    }

    @ServiceActivator(inputChannel = "psStatusChannel")
    public void handlePsStatus(KafkaPsStatusEvent kafkaPsStatusEvent) {
        PsStatusDto psStatusDto = kafkaPsStatusEvent.getProduktionssteuerungStatusDto();
        psStatusMap.put(psStatusDto.getName(), psStatusDto);

        System.out.println("Received " + kafkaPsStatusEvent.getProduktionssteuerungStatusDto().toString());

        if (assignedProduktionssteuerung == null && enablePostStartupAssigment) {
            setAssignedProduktionssteuerung();
        }

        if (psStatusDto.getName().equals(assignedProduktionssteuerung) && !psStatusDto.getWerkStatusMap().containsKey(instanceName)) {
            werkStatusEventProducerService.publishCurrentStatus(assignedProduktionssteuerung, pendingWerkAuftraege.keySet().stream().toList());
        }
    }

    @ServiceActivator(inputChannel = "psAuftragChannel")
    public void handlePsAuftrag(KafkaAuftragEvent kafkaAuftragEvent) {
        AuftragDto auftragDto = kafkaAuftragEvent.getAuftrag();
        BestellungWerkDto bestellungWerkDto = bestellungWerkEventProducerService.sendBestellungToLieferantenManagement(auftragDto);
        pendingBestellungen.put(auftragDto.getId(), bestellungWerkDto);
        auftragHinzufuegen(auftragDto);
    }

    @ServiceActivator(inputChannel = "bestellungWerkChannel")
    public void handleBestellungWerk(KafkaBestellungWerkEvent kafkaBestellungWerkEvent) {
        BestellungWerkDto bestellungWerkDto = kafkaBestellungWerkEvent.getBestellung();
        System.out.println("Bestellung erhalten: " + bestellungWerkDto.toString());
        synchronized (lock) {
            pendingBestellungen.remove(bestellungWerkDto.getAuftragId());
            receivedBestellungen.put(bestellungWerkDto.getAuftragId(), bestellungWerkDto);
            lock.notify(); // Weckt den Verarbeitungsthread, falls er wartet
        }
    }

    public void setAssignedProduktionssteuerung() {
        if (psStatusMap.isEmpty()) {
            enablePostStartupAssigment = true;
            return;
        }

        this.assignedProduktionssteuerung = psStatusMap.values().stream()
                .min(Comparator.comparingInt(psStatusMap -> psStatusMap.getWerkStatusMap().values().size())).
                map(PsStatusDto::getName).orElseThrow();

        System.out.println("Assigned Produktionssteuerung: " + this.assignedProduktionssteuerung);

        werkStatusEventProducerService.publishCurrentStatus(assignedProduktionssteuerung,
                pendingWerkAuftraege.keySet().stream().toList());
    }

    private void auftragHinzufuegen(AuftragDto auftragDto) {
        pendingWerkAuftraege.put(auftragDto.getId(), auftragDto);
    }

    private void verarbeiteAuftraege() {

        while (true) {
            try {
                AuftragDto auftragDto;
                synchronized (lock) {
                    while (receivedBestellungen.isEmpty()) {
                        try {
                            lock.wait(); // Wartet auf neue Aufträge
                        } catch (InterruptedException e) {
                            Thread.currentThread().interrupt();
                            return;
                        }
                    }
                    BestellungWerkDto bestellungWerkDto = receivedBestellungen.values().stream().findFirst().get();
                    auftragDto = pendingWerkAuftraege.get(bestellungWerkDto.getAuftragId());
                }

                if (auftragDto == null)
                    continue;

                // starte Verarbeitung des Auftrags
                System.out.println("Verarbeite Auftrag mit Id " + auftragDto.getId());

                try {
                    Thread.sleep(auftragDto.getAnzahl() * auftragDto.getProdukt().getProduktionszeit() * 1000);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    return;
                }

                // setze Auftrag auf abgeschlossen
                System.out.println("Auftrag mit Id " + auftragDto.getId() + " abgeschlossen.");
                auftragDto.setStatus("DONE");

                // entferne bearbeitete Werte
                pendingWerkAuftraege.remove(auftragDto.getId());
                receivedBestellungen.remove(auftragDto.getId());

                // veröffentliche neuen Status
                werkStatusEventProducerService.publishCurrentStatus(assignedProduktionssteuerung,
                        pendingWerkAuftraege.keySet().stream().toList());
                psAuftragEventProducerService
                        .sendAuftragToProduktionssteuerung(auftragDto, assignedProduktionssteuerung + "-auftrag");
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }


    }

}
