package de.thm.vs.efrifge.werk.kafka.producer;

import de.thm.vs.efrifge.werk.dto.AuftragDto;
import de.thm.vs.efrifge.werk.dto.BestellungWerkDto;
import de.thm.vs.efrifge.werk.kafka.config.KafkaProperties;
import de.thm.vs.efrifge.werk.kafka.events.KafkaBestellungWerkEvent;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Data
@Service
public class BestellungWerkEventProducerService {

    private final KafkaTemplate<String, KafkaBestellungWerkEvent> kafkaTemplate;
    private final KafkaProperties kafkaProperties;
    @Value("${self.name}")
    private String instanceName;

    public BestellungWerkDto sendBestellungToLieferantenManagement(AuftragDto auftragDto) {
        String uuid = UUID.randomUUID().toString();
        BestellungWerkDto bestellungWerkDto = BestellungWerkDto.builder()
                .status("PENDING")
                .auftragId(auftragDto.getId())
                .anzahl(auftragDto.getAnzahl())
                .produktId(auftragDto.getProdukt().getId())
                .werk(instanceName)
                .build();
        kafkaTemplate.send(kafkaProperties.topic().lieferantenmanagement(), KafkaBestellungWerkEvent.builder()
                .id(uuid)
                .operationType("")
                .bestellung(bestellungWerkDto)
                .build());

        return bestellungWerkDto;
    }
}
