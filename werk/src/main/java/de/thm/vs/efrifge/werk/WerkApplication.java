package de.thm.vs.efrifge.werk;

import de.thm.vs.efrifge.werk.kafka.config.KafkaProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(value = KafkaProperties.class)
public class WerkApplication {

	public static void main(String[] args) {
		SpringApplication.run(WerkApplication.class, args);
	}

}
