package de.thm.vs.efridge.lieferantenmanagement.kafka.repository;

import de.thm.vs.efridge.lieferantenmanagement.kafka.producer.ProducerCoordinator;
import jakarta.persistence.EntityManager;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.data.repository.core.RepositoryInformation;

public class EventRepositoryFactory extends JpaRepositoryFactory {

    private EntityManager entityManager;
    private ProducerCoordinator producerCoordinator;

    public EventRepositoryFactory(EntityManager entityManager, ProducerCoordinator producerCoordinator) {
        super(entityManager);
        this.entityManager = entityManager;
        this.producerCoordinator = producerCoordinator;
    }


    @NotNull
    @Override
    protected JpaRepositoryImplementation<?, ?> getTargetRepository(RepositoryInformation information, @NotNull EntityManager entityManager) {
        return new EventRepositoryImpl<>(getEntityInformation(information.getDomainType()), entityManager, producerCoordinator);
    }
}
