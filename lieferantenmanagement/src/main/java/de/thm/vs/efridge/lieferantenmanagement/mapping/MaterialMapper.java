package de.thm.vs.efridge.lieferantenmanagement.mapping;


import de.thm.vs.efridge.lieferantenmanagement.data.dto.MaterialDto;
import de.thm.vs.efridge.lieferantenmanagement.data.entities.Kategorie;
import de.thm.vs.efridge.lieferantenmanagement.data.entities.Material;
import org.mapstruct.*;

import java.util.List;

/**
 * Stellt alle Methoden zur Verfügung um ein Material zu MaterialDto zu transformieren (Mappen) und umgekehrt.
 * Enthält zusätzlich Methoden zum Transformieren eines String zu einer Kategorie und umgekehrt.
 */
@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, unmappedTargetPolicy = ReportingPolicy.IGNORE,
        uses = {ProduktMaterialMapper.class})
public interface MaterialMapper {

    @InheritInverseConfiguration
    List<MaterialDto> materialienToDtos(List<Material> materialien);

    @Mapping(source = "kategorie.name", target = "kategorie")
    List<Material> dtosToMaterialien(List<MaterialDto> materialienDtos);

    default Kategorie mapStringKategorie(String kategorie) {
        return new Kategorie(kategorie);
    }

    default String mapKategorie(Kategorie kategorie) {
        return kategorie.getName();
    }
}
