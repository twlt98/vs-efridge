package de.thm.vs.efridge.lieferantenmanagement.service;

import de.thm.vs.efridge.lieferantenmanagement.data.entities.ProduktMaterial;
import de.thm.vs.efridge.lieferantenmanagement.repository.ProduktMaterialRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * Service Class zur Abarbeitung von Produktmaterialien.
 *
 */
@Service
@RequiredArgsConstructor
public class ProduktMaterialService {

    //Repository und Services (Dienste)
    private final ProduktMaterialRepository produktMaterialRepository;

    /**
     * Ablegen einer Liste von ProduktMaterialien
     *
     * @param produktMaterialList Liste von ProduktMaterial die Abgelegt werden soll
     * @return Liste von ProduktMaterial die erfolgreich abgelegt wurden
     */
    public List<ProduktMaterial> addProduktMaterial(List<ProduktMaterial> produktMaterialList) {
        return produktMaterialRepository.saveAll(produktMaterialList);
    }
}
