package de.thm.vs.efridge.lieferantenmanagement.repository;

import de.thm.vs.efridge.lieferantenmanagement.data.entities.Bestellung;
import de.thm.vs.efridge.lieferantenmanagement.data.entities.BestellungWerk;
import de.thm.vs.efridge.lieferantenmanagement.kafka.repository.EventRepository;
import jakarta.persistence.LockModeType;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;


/**
 * EventRepository für Datenbank-Operationen im Zusammenhang mit Bestellungen.
 * Stellt alle gängigen Operationen für die Kommunikation zur Datenbank zur Verfügung.
 *
 * See also:
 *  Kafka::EventRepository
 *  SpringFramework::Repository
 */
@Repository
public interface BestellungRepository extends EventRepository<Bestellung, Long> {

    /**
     * Findet eine Liste von Bestellungen die einer BestellungWerk zugeordnet sind.
     *
     * @param bestellungWerk Ein BestellungWerk zudem Bestellungen gefunden werden sollen
     * @return Liste an Bestellungen zugeordnet zur gegebenen BestellungWerk
     */
    List<Bestellung> findByBestellungWerk(BestellungWerk bestellungWerk);


    /**
     * Findet eine Liste von Bestellungen mit einem Status und einem geringeren Zeitpunkt als dem gegebenen Zeitpunkt.
     *
     *  !Important:
     *  Beim Abholen der Dateneinträge werden diese mit einer Sperre(LOCK) versehen!
     *
     * @param status Status als String, nachdem gesucht werden soll
     * @param zeitpunkt Der Zeitpunkt als LocalDateTime der die gesuchten Werte überschreitet
     * @return
     */
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @Query("SELECT b FROM Bestellung b WHERE b.status = :status AND b.createdAt < :zeitpunkt")
    List<Bestellung> findByStatusAndCreatedAtBeforeWithLock(String status, LocalDateTime zeitpunkt);

}
