package de.thm.vs.efridge.lieferantenmanagement.data.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Schnittstellen bzw. Daten Representation einer Bestellung.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BestellungDto {
    private Long id;
    private BestellungWerkDto bestellungWerk;
    private String bestellnummer;
    private String status;
    private String lieferant;
    private List<ProduktMaterialDto> materialListe;
}
