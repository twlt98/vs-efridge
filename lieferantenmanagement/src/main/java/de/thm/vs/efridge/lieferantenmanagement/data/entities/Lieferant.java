package de.thm.vs.efridge.lieferantenmanagement.data.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Entity-Beschreibung eines Lieferanten zu Representation in der Datenbank.
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "lieferant")
public class Lieferant {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "beschreibung")
    private String beschreibung;

    //Liste an Kategorien die dieser Lieferant bedienen kann
    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(
            name = "lieferant_kategorie",
            joinColumns = @JoinColumn(name = "lieferant_id"),
            inverseJoinColumns = @JoinColumn(name = "kategorie")
    )
    private List<Kategorie> kategorien;
}
