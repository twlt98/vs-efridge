package de.thm.vs.efridge.lieferantenmanagement;

import de.thm.vs.efridge.lieferantenmanagement.kafka.config.KafkaProperties;
import de.thm.vs.efridge.lieferantenmanagement.kafka.repository.EventRepositoryFactoryBean;
import de.thm.vs.efridge.lieferantenmanagement.kafka.repository.EventRepositoryImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@EnableConfigurationProperties(KafkaProperties.class)
@EnableJpaRepositories(repositoryBaseClass = EventRepositoryImpl.class,
        repositoryFactoryBeanClass = EventRepositoryFactoryBean.class)
public class LieferantenmanagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(LieferantenmanagementApplication.class, args);
    }

}
