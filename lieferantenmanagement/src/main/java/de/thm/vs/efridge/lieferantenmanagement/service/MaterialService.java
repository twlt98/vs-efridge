package de.thm.vs.efridge.lieferantenmanagement.service;

import de.thm.vs.efridge.lieferantenmanagement.data.entities.Material;
import de.thm.vs.efridge.lieferantenmanagement.repository.MaterialRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * Service Class zur Abarbeitung von Materialien.
 *
 */
@Service
@RequiredArgsConstructor
public class MaterialService {

    //Repository und Services (Dienste)
    private final MaterialRepository materialRepository;
    private final KategorienService kategorienService;

    /**
     * Legt eine Liste an Materialien ab, inklusive deren Kategorien, sofern diese nicht schon existieren.
     *
     * @param materialien Liste von Materialien die abgelegt werden sollen, inklusive deren Kategorien.
     * @return Liste an Material die abgelegt wurden
     */
    public List<Material> addMaterialien(List<Material> materialien) {
        kategorienService.createKategorienIfNotExists(materialien.stream().map(Material::getKategorie).toList());
        return materialRepository.saveAll(materialien);
    }

}
