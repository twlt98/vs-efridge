package de.thm.vs.efridge.lieferantenmanagement.kafka.config.consumer;

import de.thm.vs.efridge.lieferantenmanagement.kafka.config.KafkaProperties;
import de.thm.vs.efridge.lieferantenmanagement.kafka.events.KafkaBestellungWerkEvent;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.kafka.support.mapping.DefaultJackson2JavaTypeMapper;
import org.springframework.kafka.support.mapping.Jackson2JavaTypeMapper;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.HashMap;
import java.util.Map;

/**
 * Kafka consumer Konfiguration für die verarbeitung von Events bezüglich BestellungWerk.
 * Erweitert KafkaConsumerConfig um weitere Konfigurationen
 */
@EnableKafka
@Configuration
public class KafkaBestellungWerkEventConsumerConfig extends KafkaConsumerConfig<KafkaBestellungWerkEvent> {

    /**
     * Konstruktor
     * @param kafkaProperties KafkaProperties als Konfigurationsmittel
     */
    public KafkaBestellungWerkEventConsumerConfig(KafkaProperties kafkaProperties) {
        super(kafkaProperties);
    }

    /**
     * Mapping und Deserialisierung von KafkaBestellungWerkEvent
     * @return DefaultKafkaConsumerFactory
     */
    @Override
    @Bean(name = "kafkaBestellungWerkEventConsumerFactory")
    public ConsumerFactory<String, KafkaBestellungWerkEvent> kafkaConsumerFactory() {
        JsonDeserializer<KafkaBestellungWerkEvent> deserializer = new JsonDeserializer<>(KafkaBestellungWerkEvent.class);
        DefaultJackson2JavaTypeMapper typeMapper = new DefaultJackson2JavaTypeMapper();
        Map<String, Class<?>> mappings = new HashMap<>();
        typeMapper.setTypePrecedence(Jackson2JavaTypeMapper.TypePrecedence.INFERRED);
        mappings.put("KafkaBestellungWerkEvent", KafkaBestellungWerkEvent.class);
        typeMapper.setIdClassMapping(mappings);
        deserializer.setTypeMapper(typeMapper);
        deserializer.addTrustedPackages("*");
        return new DefaultKafkaConsumerFactory<>(config(), new StringDeserializer(), deserializer);
    }

    /**
     * Erzeugung einer ConcurrentKafkaListenerContainerFactory für KafkaBestellungWerkEvent.
     *
     * @param kafkaConsumerFactory ConsumerFactory für KafkaBestellungWerkEvent.
     * @return ConcurrentKafkaListenerContainerFactory für KafkaBestellungWerkEvent.
     */
    @Override
    @Bean(name = "kafkaBestellungWerkEventListenerContainerFactory")
    public ConcurrentKafkaListenerContainerFactory<String, KafkaBestellungWerkEvent> kafkaListenerContainerFactory(
           @Autowired
           @Qualifier(value = "kafkaBestellungWerkEventConsumerFactory")
           ConsumerFactory<String, KafkaBestellungWerkEvent> kafkaConsumerFactory
    ) {
        ConcurrentKafkaListenerContainerFactory<String, KafkaBestellungWerkEvent> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(kafkaConsumerFactory);
        return factory;
    }
}
