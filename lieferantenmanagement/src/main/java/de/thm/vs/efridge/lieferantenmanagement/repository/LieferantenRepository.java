package de.thm.vs.efridge.lieferantenmanagement.repository;

import de.thm.vs.efridge.lieferantenmanagement.data.entities.Kategorie;
import de.thm.vs.efridge.lieferantenmanagement.data.entities.Lieferant;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository für Datenbank-Operationen im Zusammenhang mit Lieferanten.
 * Stellt alle gängigen Operationen für die Kommunikation zur Datenbank zur Verfügung.
 *
 * See also:
 *  SpringFramework::JpaRepository
 *  SpringFramework::Repository
 */
public interface LieferantenRepository extends JpaRepository<Lieferant, Long> {

    /**
     * Finden eines Lieferanten der einer Kategorie zugeordnet ist.
     *
     * @param kategorie Die Kategorie nach der ein Lieferant gesucht wird.
     * @return Einen Lieferanten der die gegebene Kategorie zugewiesen hat oder NULL
     */
    Lieferant findByKategorien(Kategorie kategorie);
}
