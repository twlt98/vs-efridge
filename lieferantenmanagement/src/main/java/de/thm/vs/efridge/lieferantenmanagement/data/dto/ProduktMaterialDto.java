package de.thm.vs.efridge.lieferantenmanagement.data.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Schnittstellen bzw. Daten Representation eines ProduktMaterials.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProduktMaterialDto {
    private Long id;
    private ProduktDto produkt;
    private MaterialDto material;
    private int anzahl;
}
