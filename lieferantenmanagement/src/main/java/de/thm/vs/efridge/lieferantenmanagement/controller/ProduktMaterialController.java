package de.thm.vs.efridge.lieferantenmanagement.controller;

import de.thm.vs.efridge.lieferantenmanagement.data.dto.ProduktMaterialDto;
import de.thm.vs.efridge.lieferantenmanagement.data.entities.ProduktMaterial;
import de.thm.vs.efridge.lieferantenmanagement.mapping.ProduktMaterialMapper;
import de.thm.vs.efridge.lieferantenmanagement.service.ProduktMaterialService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * Controller für die Verarbeitung aller REST-Anfragen bezüglich ProduktMaterialien
 */
@RestController
@RequestMapping("/produktmaterial")
@RequiredArgsConstructor
public class ProduktMaterialController {

    //Dienste (Service) und Mapper
    private final ProduktMaterialService produktMaterialService;
    private final ProduktMaterialMapper produktMaterialMapper;

    /**
     * Möglichkeit eine Liste von ProduktMaterialien via POST hinzuzufügen.
     *
     * @param produktMaterialDtos Liste von ProduktMaterialDto die hinzugefügt werden sollen
     * @return ResponsEntity mit Liste der produktMaterialDtos bei Erfolg.
     */
    @PostMapping()
    public ResponseEntity<List<ProduktMaterialDto>> addProduktMaterialien(@RequestBody List<ProduktMaterialDto> produktMaterialDtos) {
        List<ProduktMaterial> produktMaterials = produktMaterialMapper.dtosToProduktMaterial(produktMaterialDtos);
        produktMaterialDtos = produktMaterialMapper.produktMaterialToDtos(produktMaterialService.addProduktMaterial(produktMaterials));
        return ResponseEntity.ok(produktMaterialDtos);
    }
}
