package de.thm.vs.efridge.lieferantenmanagement.mapping;

import de.thm.vs.efridge.lieferantenmanagement.data.dto.ProduktDto;
import de.thm.vs.efridge.lieferantenmanagement.data.entities.Produkt;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;


/**
 * Stellt alle Methoden zur Verfügung um ein Produkt zu ProduktDto zu transformieren (Mappen) und umgekehrt.
 *
 */
@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, unmappedTargetPolicy = ReportingPolicy.IGNORE,
        uses = {ProduktMaterialMapper.class})
public interface ProduktMapper {

    /**
     * Transformieren von Produkt zu ProduktDto
     *
     * !Important:
     * Diese Methode ist als 'unused' markiert besitzt jedoch eine Abhängigkeit zu ProduktMaterialMapper.class!
     *
     * @param produkt Produkt welches zu ProduktDto transformiert werden soll
     * @return ProduktDto basierend auf produkt
     */
    ProduktDto produktToDto(Produkt produkt);


    /**
     * Transformieren von ProduktDto zu Produkt
     *
     * !Important:
     * Diese Methode ist als 'unused' markiert besitzt jedoch eine Abhängigkeit zu ProduktMaterialMapper.class!
     *
     * @param produktDtos ProduktDto welches zu Produkt transformiert werden soll
     * @return Produkt basierend auf produktDto
     */
    Produkt dtoToProdukt(ProduktDto produktDtos);
}
