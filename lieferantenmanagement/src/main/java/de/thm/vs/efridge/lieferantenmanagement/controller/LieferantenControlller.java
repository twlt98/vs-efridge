package de.thm.vs.efridge.lieferantenmanagement.controller;

import de.thm.vs.efridge.lieferantenmanagement.data.dto.LieferantDto;
import de.thm.vs.efridge.lieferantenmanagement.data.entities.Lieferant;
import de.thm.vs.efridge.lieferantenmanagement.mapping.LieferantenMapper;
import de.thm.vs.efridge.lieferantenmanagement.service.LieferantService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Controller für die Verarbeitung aller REST-Anfragen bezüglich Lieferanten
 */
@RestController
@RequestMapping("/lieferant")
@RequiredArgsConstructor
public class LieferantenControlller {

    //Dienste (Service) und Mapper
    private final LieferantService lieferantService;
    private final LieferantenMapper lieferantenMapper;

    /**
     * Möglichkeit eine Liste von Lieferanten via POST hinzuzufügen.
     *
     * @param lieferantenDtos Liste von LieferantenDtos die hinzugefügt werden sollen
     * @return ResponsEntity mit Liste der lieferantenDtos bei Erfolg.
     */
    @PostMapping()
    public ResponseEntity<List<LieferantDto>> addLieferanten(@RequestBody List<LieferantDto> lieferantenDtos) {
        List<Lieferant> lieferanten = lieferantenMapper.dtosToLieferanten(lieferantenDtos);
        lieferantenDtos = lieferantenMapper.lieferantenToDtos(lieferantService.addLieferanten(lieferanten));
        return ResponseEntity.ok(lieferantenDtos);
    }
}
