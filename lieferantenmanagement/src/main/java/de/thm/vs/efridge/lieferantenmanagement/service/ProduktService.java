package de.thm.vs.efridge.lieferantenmanagement.service;

import de.thm.vs.efridge.lieferantenmanagement.data.entities.Produkt;
import de.thm.vs.efridge.lieferantenmanagement.data.entities.Material;
import de.thm.vs.efridge.lieferantenmanagement.repository.ProduktRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * Service Class zur Abarbeitung von Produkten.
 *
 */
@Service
@RequiredArgsConstructor
public class ProduktService {

    //Repository und Services (Dienste)
    private final ProduktRepository produktRepository;

    /**
     * Abholen eines Produkts basierend auf dessen Produkt.Id
     *
     * @param id des Produkts, welches abgeholt werden soll
     * @return Produkt mit der gegebenen Produkt.Id oder throw Error
     */
    public Produkt retrieveProduktById(Long id) {
        return produktRepository.findById(id).orElseThrow();
    }
}
