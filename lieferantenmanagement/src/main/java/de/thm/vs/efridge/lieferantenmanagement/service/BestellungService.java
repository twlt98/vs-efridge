package de.thm.vs.efridge.lieferantenmanagement.service;

import de.thm.vs.efridge.lieferantenmanagement.data.entities.*;
import de.thm.vs.efridge.lieferantenmanagement.repository.BestellungRepository;
import de.thm.vs.efridge.lieferantenmanagement.repository.BestellungWerkRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.hibernate.exception.LockTimeoutException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Service Class zur Abarbeitung von Bestellungen.
 *
 * !Important:
 * Diese Klasse nutzt die Klasse BestellungWerkRepository, um auf Datenbankeinträge für BestellungWerk zuzugreifen.
 * Damit wird der BestellungWerkService umgangen.
 */
@Service
@RequiredArgsConstructor
public class BestellungService {

    //Repository und Services (Dienste)
    private final BestellungRepository bestellungRepository;
    private final BestellungWerkRepository bestellungWerkRepository;
    private final ProduktService produktService;
    private final LieferantService lieferantService;

    //Logging/Protokollierung mit Log4j
    private static final Logger logger = LoggerFactory.getLogger(BestellungService.class);

    //ENUM zur wahrung der Konsistenz des BestellStatus
    enum BestellungStatus {
        PENDING,
        DELIVERED
    }

    /**
     * Fügt eine Liste an Bestellungen über das repository in die Datenbank hinzu.
     *
     * @param bestellung Liste der Bestellungen die hinzugefügt werden sollen.
     * @return Liste von Bestellungen die hinzugefügt wurden.
     */
    public List<Bestellung> addBestellungen(List<Bestellung> bestellung) {
        return bestellungRepository.saveAll(bestellung);
    }

    /**
     * Abarbeitung einer BestellungWerk zur bestellung bei einzelnen Händlern.
     *
     * !Important:
     * Die übergebene BestellungWerk benötigt ein Produkt mit einer gültigen Produkt.id
     *
     * @param bestellungWerk Eine BestellungWerk mit entsprechendem Produkt mit einer Produkt.id.
     */
    @Transactional
    public void bestellungenByBestellungWerk(BestellungWerk bestellungWerk) {
        List<Bestellung> bestellungen = splitBestellungWerk(bestellungWerk);
        addBestellungen(bestellungen);
        // ****
        // Simulationsanfang des aufgebens der Bestellung
        verarbeiteBestellungen(bestellungen);
        // Simulationsende des aufgebens der Bestellung
        // ****
    }

    /**
     * Aufteilen der notwendigen Materialien eines Produktes aus BestellungWerk auf einzelne Bestellungen bei Lieferanten
     * Zuordnen der Materialien auf Bestellungen basieren auf der Materialkategorie und den Lieferanten Kategorien die dieser bedienen kann.
     *
     * Nutzt ProduktService, um ein Produkt zu erhalten basierend auf der Produkt.id in BestellungWerk.
     * Die darin vorhandene Liste aus Produktmaterialien wird infolgedessen abgelaufen und passende Lieferanten mithilfe des LieferantenServices gefunden.
     *
     * @param bestellungWerk Eine BestellungWerk mit entsprechendem Produkt mit einer Produkt.id.
     * @return Liste von Bestellungen erzeugt basierend auf der Teilung durch Lieferanten, ProduktMaterialien und dessen Kategorien
     */
    private List<Bestellung> splitBestellungWerk(BestellungWerk bestellungWerk) {
        Produkt produkt = produktService.retrieveProduktById(bestellungWerk.getProdukt().getId());
        Map<Lieferant, List<ProduktMaterial>> lieferantenMap = new HashMap<>();

        for (ProduktMaterial produktMaterial : produkt.getMaterial()) {
            produktMaterial.setAnzahl(produktMaterial.getAnzahl() * bestellungWerk.getAnzahl());
            Lieferant lieferant = lieferantService.retrieveLieferantByKategorie(produktMaterial.getMaterial().getKategorie());
            if(!lieferantenMap.containsKey(lieferant))
                lieferantenMap.put(lieferant, new ArrayList<>());
            lieferantenMap.get(lieferant).add(produktMaterial);
        }

        List<Bestellung> bestellungen = new ArrayList<>();
        for (Map.Entry<Lieferant, List<ProduktMaterial>> entry : lieferantenMap.entrySet())
            bestellungen.add(new Bestellung(null, bestellungWerk, UUID.randomUUID().toString(), BestellungStatus.PENDING.name(), entry.getKey(), null, entry.getValue()));
        return bestellungen;
    }

    /**
     * ***************************************************************************************************
     * SIMULATION
     * ***************************************************************************************************
     */

    /**
     * Simuliert die Verarbeitung einer Liste von Bestellungen.
     * Nach einem Ablauf von x Millisekunden werden diese auf DELIVERED gesetzt.
     * Und der Status in der Datenbank aktuellisiert.
     *
     * @param bestellungen Liste an Bestellungen die Simuliet werden sollen
     */
    @Transactional
    protected void verarbeiteBestellungen(List<Bestellung> bestellungen) {
            bestellungen.forEach(bestellung -> logger.info("Verarbeite Bestellungen: " + bestellung.getId()));

            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                return;
            }

            bestellungen.forEach(bestellung -> bestellung.setStatus(BestellungStatus.DELIVERED.name()));
            bestellungen = bestellungRepository.saveAllAndPublish(bestellungen);

            bestellungen.forEach(bestellung -> logger.info("Bestellungen erhalten: " + bestellung.getId()));

            Set<BestellungWerk> bestellungenWerk = bestellungen.stream().map(Bestellung::getBestellungWerk).collect(Collectors.toSet());

            Map<BestellungWerk, List<Bestellung>> bestellungenMap = new HashMap<>();
            bestellungenWerk.forEach(bestellungWerk -> {
                bestellungenMap.put(bestellungWerk, bestellungRepository.findByBestellungWerk(bestellungWerk));
            });

            checkBestellungenWerk(bestellungenMap);
    }

    /**
     * Überprüft den Lieferstatus einer BestellungWerk basierend auf den Lieferstatie der dazugehörigen Bestellungen
     * Zu einer BestellungWerk gehören eine oder mehrere Bestellungen die unterschiedlich Lieferstati aufweisen können.
     * Eine BestellungWerk ist nur dann DELIVERED, wenn alle dazugehörenden Bestellungen ebenfalls DELIVERED gesetzt sind.
     *
     * @param bestellungenMap Eine HashMap mit BestellungWerk (Key) und den dazugehörigen Bestellungen als Liste (Value)
     */
    @Transactional
    protected void checkBestellungenWerk(Map<BestellungWerk, List<Bestellung>> bestellungenMap) {
        logger.info("Aktualisiere Bestellungen von Werk!");
        bestellungenMap.forEach((key, value) -> {
            if (value.stream().noneMatch(bestellung -> bestellung.getStatus().equals(BestellungStatus.PENDING.name()))) {
                key.setStatus(BestellungStatus.DELIVERED.name());
                bestellungWerkRepository.saveAndPublish(key);
                logger.info("Bestellung für Werk geliefert: " + key.getId());
            }
        });
    }

    /**
     * Abholen aus der Datenbank und Abarbeitung alle Bestellungen die auf PENDING stehen.
     * Wird alle 30.000 Millisekunden ausgeführt und dient dem Anstoßen der Verarbeitung, falls eine oder mehrere
     * Lieferantenmanagements ausfallen oder sich Neustarten.
     *
     */
    @Transactional
    @Scheduled(fixedRate = 30000)
    public void pruefePendingBestellungen() {
        try {
            logger.info("Job execution for dangling Bestellungen.");
            LocalDateTime vorDreißigSekunden = LocalDateTime.now().minusSeconds(30);
            List<Bestellung> pendingBestellungen = bestellungRepository.findByStatusAndCreatedAtBeforeWithLock(BestellungStatus.PENDING.name(), vorDreißigSekunden);
            if (!pendingBestellungen.isEmpty())
                verarbeiteBestellungen(pendingBestellungen);
        } catch (LockTimeoutException e) {
            logger.warn("Job executed from another Instance");
        } catch (Exception e) {
            logger.error("Error executing the scheduled task", e);
        }
    }
}
