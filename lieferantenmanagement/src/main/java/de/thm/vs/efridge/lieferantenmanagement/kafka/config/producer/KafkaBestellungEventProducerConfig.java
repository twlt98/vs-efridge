package de.thm.vs.efridge.lieferantenmanagement.kafka.config.producer;

import de.thm.vs.efridge.lieferantenmanagement.kafka.config.KafkaProperties;
import de.thm.vs.efridge.lieferantenmanagement.kafka.events.KafkaBestellungEvent;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KafkaBestellungEventProducerConfig extends KafkaProducerConfig<KafkaBestellungEvent> {

    public KafkaBestellungEventProducerConfig(KafkaProperties kafkaProperties) {
        super(kafkaProperties);
    }
}
