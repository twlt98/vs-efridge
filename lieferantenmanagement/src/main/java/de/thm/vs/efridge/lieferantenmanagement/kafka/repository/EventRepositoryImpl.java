package de.thm.vs.efridge.lieferantenmanagement.kafka.repository;

import de.thm.vs.efridge.lieferantenmanagement.kafka.producer.ProducerCoordinator;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class EventRepositoryImpl<T, ID extends Serializable> extends SimpleJpaRepository<T, ID>
        implements EventRepository<T, ID> {

    private ProducerCoordinator producerCoordinator;

    public EventRepositoryImpl(JpaEntityInformation<T, ID> entityInformation, EntityManager entityManager, ProducerCoordinator producerCoordinator) {
        super(entityInformation, entityManager);
        this.producerCoordinator = producerCoordinator;
    }

    @NotNull
    @Override
    @Transactional
    public <S extends T> S saveAndPublish(S entity) {
        entity = saveAndFlush(entity);
        producerCoordinator.publishEvent(entity);
        return entity;
    }

    @NotNull
    @Override
    @Transactional
    public <S extends T> List<S> saveAllAndPublish(Iterable<S> entities) {
        List<S> result = new ArrayList<>();
        for (S entity : entities) {
            entity = saveAndFlush(entity);
            producerCoordinator.publishEvent(entity);
            result.add(saveAndFlush(entity));
        }
        return result;
    }

    @NotNull
    @Override
    @Transactional
    public T updateAndPublish(T entity) {
        producerCoordinator.publishEvent(entity);
        return saveAndFlush(entity);
    }
}
