package de.thm.vs.efridge.lieferantenmanagement.data.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Schnittstellen bzw. Daten Representation eines Lieferanten.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LieferantDto {

    private String name;

    private String beschreibung;

    private List<String> kategorien;

}
