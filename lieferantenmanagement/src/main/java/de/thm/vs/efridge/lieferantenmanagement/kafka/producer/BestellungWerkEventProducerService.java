package de.thm.vs.efridge.lieferantenmanagement.kafka.producer;

import de.thm.vs.efridge.lieferantenmanagement.data.dto.BestellungWerkDto;
import de.thm.vs.efridge.lieferantenmanagement.kafka.events.KafkaBestellungWerkEvent;
import lombok.Data;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@Service
public class BestellungWerkEventProducerService {

    private final KafkaTemplate<String, KafkaBestellungWerkEvent> kafkaTemplate;

    public void sendBestellungToWerk(BestellungWerkDto bestellungWerkDto, String werkTopic) {
        String uuid = UUID.randomUUID().toString();

        kafkaTemplate.send(werkTopic, KafkaBestellungWerkEvent.builder()
                .id(uuid)
                .operationType("")
                .bestellung(bestellungWerkDto)
                .build());


    }
}
