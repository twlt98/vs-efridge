package de.thm.vs.efridge.lieferantenmanagement.mapping;

import de.thm.vs.efridge.lieferantenmanagement.data.dto.BestellungDto;
import de.thm.vs.efridge.lieferantenmanagement.data.entities.Bestellung;
import org.mapstruct.*;


/**
 * Stellt alle Methoden zur Verfügung um eine Bestellung zu BestellungDto zu transformieren (Mappen) und umgekehrt.
 */
@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, unmappedTargetPolicy = ReportingPolicy.IGNORE,
        uses = {BestellungWerkMapper.class, ProduktMaterialMapper.class})
public interface BestellungMapper {

    @Mapping(source = "lieferant.name", target = "lieferant")
    BestellungDto bestellungToDto(Bestellung bestellung);

}
