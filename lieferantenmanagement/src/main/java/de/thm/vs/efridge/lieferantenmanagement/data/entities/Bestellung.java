package de.thm.vs.efridge.lieferantenmanagement.data.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Entity-Beschreibung einer Bestellung zu Representation in der Datenbank.
 * Bestellungen sind Einkäufe bei Lieferanten. Diese werden,
 * basierend auf den benötigten Materialien die zur herstellung eines Produkts benötigt werden,
 * aus einer Bestellung eines Werks (BestellungWerk) erzeugt.
 * Aus einer BestellungWerk resultieren entsprechend Bestellungen die dieser zugeordnet werden.
 *
 */
@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "bestellung")
public class Bestellung {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //Zuordnung aus welcher BestellungWerk diese Bestellung resultiert.
    @ManyToOne
    @JoinColumn(name = "bestellungWerkId")
    private BestellungWerk bestellungWerk;

    //Die Bestellnummer, die durch den jeweiligen Händler kommuniziert wird.
    @Column(name = "bestellnummer")
    private String bestellnummer;

    //Der Status der Bestellung (bspw. PENDING, DELIVERED).
    @Column(name = "status")
    private String status;

    //Der Lieferant bei dem die Bestellung aufgeben wurde/wird.
    @ManyToOne
    @JoinColumn(name = "lieferantId")
    private Lieferant lieferant;

    //Zeitstempel wann die Bestellung erzeugt wurde.
    @CreationTimestamp
    @Column(updatable = false)
    private LocalDateTime createdAt;

    //Liste ProduktMaterial bzw. Materialien die bestellt wurden und dessen Anzahl.
    //Zuordnung einer Bestellung zu ProduktMaterialien
    @ManyToMany
    @JoinTable(
            name = "bestellung_produktmaterial",
            joinColumns = @JoinColumn(name = "bestellung_id"),
            inverseJoinColumns = @JoinColumn(name = "produktmaterial_id")
    )
    private List<ProduktMaterial> materialListe;

}
