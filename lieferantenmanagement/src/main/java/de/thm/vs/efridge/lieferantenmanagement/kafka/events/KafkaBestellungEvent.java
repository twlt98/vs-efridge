package de.thm.vs.efridge.lieferantenmanagement.kafka.events;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import de.thm.vs.efridge.lieferantenmanagement.data.dto.BestellungDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonTypeName(value = "KafkaBestellungEvent")
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
public class KafkaBestellungEvent extends KafkaEvent {
    private BestellungDto bestellung;
}
