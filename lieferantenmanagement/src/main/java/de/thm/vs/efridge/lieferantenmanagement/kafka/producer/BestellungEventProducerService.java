package de.thm.vs.efridge.lieferantenmanagement.kafka.producer;

import de.thm.vs.efridge.lieferantenmanagement.data.dto.BestellungDto;
import de.thm.vs.efridge.lieferantenmanagement.kafka.events.KafkaBestellungEvent;
import lombok.Data;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Data
@Service
public class BestellungEventProducerService {

    private final KafkaTemplate<String, KafkaBestellungEvent> kafkaTemplate;

    public void sendBestellungToTopic(BestellungDto bestellungDto) {
        String uuid = UUID.randomUUID().toString();

        kafkaTemplate.send("bestellung", KafkaBestellungEvent.builder()
                .id(uuid)
                .operationType("")
                .bestellung(bestellungDto)
                .build());


    }
}
