package de.thm.vs.efridge.lieferantenmanagement.service;

import de.thm.vs.efridge.lieferantenmanagement.data.entities.Kategorie;
import de.thm.vs.efridge.lieferantenmanagement.repository.KategorieRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service Class zur Abarbeitung von BestellungWerk.
 *
 */
@Service
@RequiredArgsConstructor
public class KategorienService {

    //Repository und Services (Dienste)
    private final KategorieRepository kategorieRepository;

    /**
     * Ablegen von kategorien in der Datenbank, sofern diese noch nicht existieren.
     *
     * @param kategorien Liste an Kategorien die angelegt werden sollen.
     * @return Liste an Kategorien die hinzugefügt wurden.
     */
    public List<Kategorie> createKategorienIfNotExists(List<Kategorie> kategorien) {
        return kategorien.stream().map(this::createKategorieIfNotExists).toList();
    }

    /**
     * Ablegen einer Kategorie in deer Datenbank, sofern diese noch nicht existiert.
     *
     * @param kategorie Kategorie die angelegt werden soll
     * @return Kategorie die angelegt werden soll
     */
    public Kategorie createKategorieIfNotExists(Kategorie kategorie) {
        if (!kategorieRepository.existsByName(kategorie.getName())) {
           kategorieRepository.saveAndFlush(kategorie);
        }
        return kategorie;
    }
}
