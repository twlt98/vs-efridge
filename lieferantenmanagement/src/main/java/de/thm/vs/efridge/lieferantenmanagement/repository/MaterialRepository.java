package de.thm.vs.efridge.lieferantenmanagement.repository;

import de.thm.vs.efridge.lieferantenmanagement.data.entities.Material;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository für Datenbank-Operationen im Zusammenhang mit Materialien.
 * Stellt alle gängigen Operationen für die Kommunikation zur Datenbank zur Verfügung.
 *
 * See also:
 *  SpringFramework::JpaRepository
 *  SpringFramework::Repository
 */
@Repository
public interface MaterialRepository extends JpaRepository<Material, Long> {
}
