package de.thm.vs.efridge.lieferantenmanagement.data.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Schnittstellen bzw. Daten Representation eines Materials.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MaterialDto {
    private Long id;
    private String name;
    private String beschreibung;
    private String kategorie;
    private double preis;
}