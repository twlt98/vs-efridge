package de.thm.vs.efridge.lieferantenmanagement.kafka.producer;

import de.thm.vs.efridge.lieferantenmanagement.data.dto.BestellungDto;
import de.thm.vs.efridge.lieferantenmanagement.data.dto.BestellungWerkDto;
import de.thm.vs.efridge.lieferantenmanagement.data.entities.Bestellung;
import de.thm.vs.efridge.lieferantenmanagement.data.entities.BestellungWerk;
import de.thm.vs.efridge.lieferantenmanagement.mapping.BestellungMapper;
import de.thm.vs.efridge.lieferantenmanagement.mapping.BestellungWerkMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ProducerCoordinator {

    private final BestellungWerkEventProducerService bestellungWerkEventProducerService;
    private final BestellungWerkMapper bestellungWerkMapper;

    private final BestellungEventProducerService bestellungEventProducerService;
    private final BestellungMapper bestellungMapper;

    public void publishEvent(Object obj) {
        if (obj instanceof BestellungWerk) {
            BestellungWerkDto bestellungWerkDto = bestellungWerkMapper.bestellungWerkToDto((BestellungWerk) obj);
            bestellungWerkEventProducerService
                    .sendBestellungToWerk(bestellungWerkDto, bestellungWerkDto.getWerk() + "-bestellung");
        }

        if (obj instanceof Bestellung) {
            BestellungDto bestellungDto = bestellungMapper.bestellungToDto((Bestellung) obj);
            bestellungEventProducerService.sendBestellungToTopic(bestellungDto);
        }
    }
}
