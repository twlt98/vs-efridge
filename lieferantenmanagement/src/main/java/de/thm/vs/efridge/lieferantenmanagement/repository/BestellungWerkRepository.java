package de.thm.vs.efridge.lieferantenmanagement.repository;

import de.thm.vs.efridge.lieferantenmanagement.data.entities.BestellungWerk;
import de.thm.vs.efridge.lieferantenmanagement.kafka.repository.EventRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * EventRepository für Datenbank-Operationen im Zusammenhang mit BestellungWerk.
 * Stellt alle gängigen Operationen für die Kommunikation zur Datenbank zur Verfügung.
 *
 * See also:
 *  Kafka::EventRepository
 *  SpringFramework::Repository
 */
@Repository
public interface BestellungWerkRepository extends EventRepository<BestellungWerk, Long> {
}
