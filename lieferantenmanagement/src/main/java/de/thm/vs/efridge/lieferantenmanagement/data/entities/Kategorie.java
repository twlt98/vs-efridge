package de.thm.vs.efridge.lieferantenmanagement.data.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Entity-Beschreibung einer Kategorie zu Representation in der Datenbank.
 * Kategorien werden Materialien zugewiesen und genutzt, um einem Lieferanten zuzuordnen,
 * welche Kategorie an Material dieser liefern kann.
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "kategorie")
public class Kategorie {

    @Id
    private String name;

}
