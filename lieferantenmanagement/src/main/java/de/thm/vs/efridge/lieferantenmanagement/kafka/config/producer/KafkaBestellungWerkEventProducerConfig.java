package de.thm.vs.efridge.lieferantenmanagement.kafka.config.producer;

import de.thm.vs.efridge.lieferantenmanagement.kafka.config.KafkaProperties;
import de.thm.vs.efridge.lieferantenmanagement.kafka.events.KafkaBestellungWerkEvent;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KafkaBestellungWerkEventProducerConfig extends KafkaProducerConfig<KafkaBestellungWerkEvent> {

    public KafkaBestellungWerkEventProducerConfig(KafkaProperties kafkaProperties) {
        super(kafkaProperties);
    }
}
