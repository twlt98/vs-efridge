package de.thm.vs.efridge.lieferantenmanagement.data.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Schnittstellen bzw. Daten Representation einer BestellungWerk.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor

public class BestellungWerkDto {
    private Long auftragId;
    private String status;
    private String werk;
    private Long produktId;
    private Long anzahl;
}
