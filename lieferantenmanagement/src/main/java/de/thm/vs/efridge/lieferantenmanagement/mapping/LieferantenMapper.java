package de.thm.vs.efridge.lieferantenmanagement.mapping;

import de.thm.vs.efridge.lieferantenmanagement.data.dto.LieferantDto;
import de.thm.vs.efridge.lieferantenmanagement.data.entities.Kategorie;
import de.thm.vs.efridge.lieferantenmanagement.data.entities.Lieferant;
import org.mapstruct.*;

import java.util.List;


/**
 * Stellt alle Methoden zur Verfügung um ein Lieferant zu LieferantDto zu transformieren (Mappen) und umgekehrt.
 * Enthält zusätzlich Methoden zum Transformieren von List<String> zu einer Liste von Kategorie und umgekehrt.
 */
@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface LieferantenMapper {

    @Mapping(target = "kategorien", qualifiedByName = "kategorienToStringKategorien")
    LieferantDto lieferantToDto(Lieferant lieferant);

    @Mapping(target = "kategorien", qualifiedByName = "kategorienStringsToKategorien")
    Lieferant dtoToLieferant(LieferantDto lieferantDtos);
    List<Lieferant> dtosToLieferanten(List<LieferantDto> dtos);
    List<LieferantDto> lieferantenToDtos(List<Lieferant> lieferanten);

    @Named("kategorienToStringKategorien")
    default List<String> kategorienToStringKategorien(List<Kategorie> kategorien) {
        return kategorien.stream().map(Kategorie::getName).toList();
    }

    @Named("kategorienStringsToKategorien")
    default List<Kategorie> kategorienStringsKategorien(List<String> kategorien) {
        return kategorien.stream().map(Kategorie::new).toList();
    }
}
