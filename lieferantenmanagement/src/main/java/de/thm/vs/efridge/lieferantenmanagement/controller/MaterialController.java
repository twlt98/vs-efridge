package de.thm.vs.efridge.lieferantenmanagement.controller;

import de.thm.vs.efridge.lieferantenmanagement.data.dto.MaterialDto;
import de.thm.vs.efridge.lieferantenmanagement.data.dto.ProduktDto;
import de.thm.vs.efridge.lieferantenmanagement.data.entities.Material;
import de.thm.vs.efridge.lieferantenmanagement.data.entities.Produkt;
import de.thm.vs.efridge.lieferantenmanagement.mapping.MaterialMapper;
import de.thm.vs.efridge.lieferantenmanagement.mapping.ProduktMapper;
import de.thm.vs.efridge.lieferantenmanagement.service.MaterialService;
import de.thm.vs.efridge.lieferantenmanagement.service.ProduktService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * Controller für die Verarbeitung aller REST-Anfragen bezüglich Materialien
 */
@RestController
@RequestMapping("/material")
@RequiredArgsConstructor
public class MaterialController {

    //Dienste (Service) und Mapper
    private final MaterialService materialService;
    private final MaterialMapper materialMapper;

    /**
     * Möglichkeit eine Liste von Materialien via POST hinzuzufügen.
     *
     * @param materialDtos Liste von MaterialDto die hinzugefügt werden sollen
     * @return ResponsEntity mit Liste der materialDtos bei Erfolg.
     */
    @PostMapping()
    public ResponseEntity<List<MaterialDto>> addMaterialien(@RequestBody List<MaterialDto> materialDtos) {
        List<Material> materialien = materialMapper.dtosToMaterialien(materialDtos);
        materialDtos = materialMapper.materialienToDtos(materialService.addMaterialien(materialien));
        return ResponseEntity.ok(materialDtos);
    }
}
