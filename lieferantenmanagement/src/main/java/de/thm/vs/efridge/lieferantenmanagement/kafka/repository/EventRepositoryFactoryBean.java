package de.thm.vs.efridge.lieferantenmanagement.kafka.repository;

import de.thm.vs.efridge.lieferantenmanagement.kafka.producer.ProducerCoordinator;
import jakarta.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactoryBean;
import org.springframework.data.repository.core.support.RepositoryFactorySupport;
import org.springframework.stereotype.Component;

import java.io.Serializable;

public class EventRepositoryFactoryBean<T extends JpaRepository<S, ID>, S, ID extends Serializable> extends JpaRepositoryFactoryBean<T, S, ID> {

    @Autowired
    private ProducerCoordinator producerCoordinator;
    public EventRepositoryFactoryBean(Class repositoryInterface) {
        super(repositoryInterface);
    }
    @Override
    protected RepositoryFactorySupport createRepositoryFactory(EntityManager entityManager) {
        return new EventRepositoryFactory(entityManager, producerCoordinator);
    }
}
