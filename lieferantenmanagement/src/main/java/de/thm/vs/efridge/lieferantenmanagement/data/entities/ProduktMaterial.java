package de.thm.vs.efridge.lieferantenmanagement.data.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * Entity-Beschreibung eines ProduktMaterials zu Representation in der Datenbank.
 * ProduktMaterial wird genutzt, um eine Beziehung zwischen Produkt und Material mit dem Zusatz einer Anzahl herzustellen.
 * Wird ebenfalls als Beziehung zwischen einer Bestellung und Material genutzt.
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "produktMaterial")
public class ProduktMaterial {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "produkt_id")
    private Produkt produkt;

    @ManyToOne
    @JoinColumn(name = "material_id")
    private Material material;

    @Column(name = "anzahl")
    private int anzahl;
}

