package de.thm.vs.efridge.lieferantenmanagement.mapping;

import de.thm.vs.efridge.lieferantenmanagement.data.dto.BestellungWerkDto;
import de.thm.vs.efridge.lieferantenmanagement.data.entities.BestellungWerk;
import org.mapstruct.*;

import java.util.List;


/**
 * Stellt alle Methoden zur Verfügung um eine BestellungWerk zu BestellungWerkDto zu transformieren (Mappen) und umgekehrt.
 */
@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface BestellungWerkMapper {
    BestellungWerkDto bestellungWerkToDto(BestellungWerk bestellungWerk);

    @Mappings(
            @Mapping(source = "produktId", target = "produkt.id")
    )
    BestellungWerk dtoToBestellungWerk(BestellungWerkDto bestellungWerkDto);
}
