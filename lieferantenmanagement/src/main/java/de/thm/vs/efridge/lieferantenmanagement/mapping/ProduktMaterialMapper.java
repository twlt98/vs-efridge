package de.thm.vs.efridge.lieferantenmanagement.mapping;

import de.thm.vs.efridge.lieferantenmanagement.data.dto.ProduktMaterialDto;
import de.thm.vs.efridge.lieferantenmanagement.data.entities.ProduktMaterial;

import org.mapstruct.*;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * Stellt alle Methoden zur Verfügung um ein ProduktMaterial zu ProduktMaterialDto zu transformieren (Mappen) und umgekehrt.
 */
@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, unmappedTargetPolicy = ReportingPolicy.IGNORE,
        uses = {MaterialMapper.class, ProduktMapper.class})
public interface ProduktMaterialMapper {
    List<ProduktMaterialDto> produktMaterialToDtos(List<ProduktMaterial> produktMaterial);

    List<ProduktMaterial> dtosToProduktMaterial(List<ProduktMaterialDto> produktMaterialDtos);

    ProduktMaterial dtoToProduktMaterial(ProduktMaterialDto produktMaterialDto);

    ProduktMaterialDto produktMaterialToDto(ProduktMaterial produktMaterial);

}
