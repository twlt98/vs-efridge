package de.thm.vs.efridge.lieferantenmanagement.repository;

import de.thm.vs.efridge.lieferantenmanagement.data.entities.Kategorie;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository für Datenbank-Operationen im Zusammenhang mit Kategorien.
 * Stellt alle gängigen Operationen für die Kommunikation zur Datenbank zur Verfügung.
 *
 * See also:
 *  SpringFramework::JpaRepository
 *  SpringFramework::Repository
 */
public interface KategorieRepository extends JpaRepository<Kategorie, Long> {

    /**
     * Überprüft, ob eine Kategorie bereit in der Datenbank existiert.
     *
     * @param name Name der Kategorie als String
     * @return true, wenn die Kategorie existiert, sonnt false
     */
    boolean existsByName(String name);
}

