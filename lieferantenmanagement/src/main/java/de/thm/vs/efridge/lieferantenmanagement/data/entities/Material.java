package de.thm.vs.efridge.lieferantenmanagement.data.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Entity-Beschreibung eines Materials zu Representation in der Datenbank.
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "material")
public class Material {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "beschreibung")
    private String beschreibung;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "kategorie")
    private Kategorie kategorie;

    @Column(name = "preis")
    private double preis;

    @OneToMany(mappedBy = "material", cascade = CascadeType.ALL)
    private List<ProduktMaterial> produkte;
}
