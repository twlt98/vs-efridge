package de.thm.vs.efridge.lieferantenmanagement.service;

import de.thm.vs.efridge.lieferantenmanagement.data.dto.BestellungWerkDto;
import de.thm.vs.efridge.lieferantenmanagement.data.entities.BestellungWerk;
import de.thm.vs.efridge.lieferantenmanagement.kafka.events.KafkaBestellungWerkEvent;
import de.thm.vs.efridge.lieferantenmanagement.mapping.BestellungWerkMapper;
import de.thm.vs.efridge.lieferantenmanagement.repository.BestellungWerkRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Service;


/**
 * Service Class zur Abarbeitung von BestellungWerk.
 *
 */
@Service
@RequiredArgsConstructor
public class BestellungWerkService {

    //Repository und Services (Dienste)
    private final BestellungWerkRepository bestellungWerkRepository;
    private final BestellungService bestellungService;
    private final BestellungWerkMapper bestellungWerkMapper;

    //Logging/Protokollierung mit Log4j
    private static final Logger logger = LoggerFactory.getLogger(BestellungWerkService.class);

    /**
     * Abarbeitung/Entgegennehmen eines KafkaBestellungWerkEvents.
     * Erwartet auf dem inputChannel "bestellungChannel" Events der sorte KafkaBestellungWerkEvents.
     * Die darin enthaltene BestellungWerk wird infolge weiterverarbeitet.
     *
     * See also:
     *  KafkaBestellungWerkEvents
     *
     * @param kafkaBestellungWerkEvent Ein KafkaBestellungWerkEvents erwartet auf dem inputChannel "bestellungChannel"
     */
    @Transactional
    @ServiceActivator(inputChannel = "bestellungChannel")
    public void handleBestellungWerk(KafkaBestellungWerkEvent kafkaBestellungWerkEvent) {
        logger.info("KafkaBestellungWerkEvent: eingegangen!");
        BestellungWerkDto bestellungWerkDto = kafkaBestellungWerkEvent.getBestellung();
        BestellungWerk bestellungWerk = bestellungWerkMapper.dtoToBestellungWerk(bestellungWerkDto);
        bestellungWerk = bestellungWerkRepository.save(bestellungWerk);
        bestellungService.bestellungenByBestellungWerk(bestellungWerk);
        logger.info("KafkaBestellungWerkEvent: abgearbeitet!");
    }
}
