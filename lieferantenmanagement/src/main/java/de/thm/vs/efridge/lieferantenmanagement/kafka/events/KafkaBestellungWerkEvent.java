package de.thm.vs.efridge.lieferantenmanagement.kafka.events;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import de.thm.vs.efridge.lieferantenmanagement.data.dto.BestellungWerkDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonTypeName(value = "KafkaBestellungWerkEvent")
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
public class KafkaBestellungWerkEvent extends KafkaEvent {
    private BestellungWerkDto bestellung;
}
