package de.thm.vs.efridge.lieferantenmanagement.kafka.config;

import de.thm.vs.efridge.lieferantenmanagement.kafka.events.KafkaBestellungWerkEvent;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.kafka.dsl.Kafka;
import org.springframework.kafka.core.ConsumerFactory;

@Configuration
@EnableIntegration
@RequiredArgsConstructor
public class KafkaIntegrationConfig {

    private final KafkaProperties kafkaProperties;
    @Qualifier(value = "kafkaBestellungWerkEventConsumerFactory")
    private final ConsumerFactory<String, KafkaBestellungWerkEvent> bestellungWerkConsumerFactory;

    @Bean
    public IntegrationFlow bestellungWerkFlow() {
        return IntegrationFlow
                .from(Kafka.messageDrivenChannelAdapter(bestellungWerkConsumerFactory,
                        kafkaProperties.topic().lieferantenmanagement()))
                .channel("bestellungChannel")
                .get();
    }

    // Weitere Kanäle und Konfigurationen
}
