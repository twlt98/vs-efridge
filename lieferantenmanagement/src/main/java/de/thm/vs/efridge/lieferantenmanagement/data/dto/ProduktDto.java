package de.thm.vs.efridge.lieferantenmanagement.data.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


/**
 * Schnittstellen bzw. Daten Representation eines Produkts.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProduktDto {
    private Long id;
    private String name;
    private String beschreibung;
    private double gewicht;
    private String dim;
    private Long produktionszeit;
}
