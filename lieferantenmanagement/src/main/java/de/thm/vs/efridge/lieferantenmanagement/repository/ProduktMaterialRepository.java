package de.thm.vs.efridge.lieferantenmanagement.repository;

import de.thm.vs.efridge.lieferantenmanagement.data.entities.ProduktMaterial;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository für Datenbank-Operationen im Zusammenhang mit ProduktMaterial.
 * Stellt alle gängigen Operationen für die Kommunikation zur Datenbank zur Verfügung.
 *
 * See also:
 *  SpringFramework::JpaRepository
 *  SpringFramework::Repository
 */
@Repository
public interface ProduktMaterialRepository extends JpaRepository<ProduktMaterial, Long>{
}
