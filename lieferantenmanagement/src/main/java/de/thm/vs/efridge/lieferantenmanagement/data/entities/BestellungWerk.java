package de.thm.vs.efridge.lieferantenmanagement.data.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Entity-Beschreibung einer BestellungWerk zu Representation in der Datenbank.
 * BestellungWerk sind interne Bestellungen die durch ein Werk veranlasst werden.
 * Sie Dienen der Kommunikation zwischen Werk und Lieferantenmanagement haben jedoch keine direkte Bedeutung für einen Lieferanten.
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "bestellungWerk")
public class BestellungWerk {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //Auftragsnummer/ID die dieser BestellungWerk zugeordnet ist.
    //Aufträge sind durch einen Kunden erzeugte Produktionsaufträge.
    @Column(name = "auftragId")
    private Long auftragId;

    //Der Status der Bestellung (bspw. PENDING, DELIVERED).
    @Column(name = "status")
    private String status;

    //Identifikation des Werks, welches die Bestellung aufgegeben hat.
    //Wird auch genutzt, um das entsprechende Topic des Werks zu bedienen.
    @Column(name = "werk")
    private String werk;

    //Das Produkt, welches produziert werden soll zudem Materialien benötigt werden.
    @ManyToOne
    @JoinColumn(name = "produkt_id")
    private Produkt produkt;

    //Anzahl wie häufig das Produkt produziert werden soll.
    @Column(name = "anzahl")
    private int anzahl;
}
