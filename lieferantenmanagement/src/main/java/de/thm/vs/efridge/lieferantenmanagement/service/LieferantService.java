package de.thm.vs.efridge.lieferantenmanagement.service;

import de.thm.vs.efridge.lieferantenmanagement.data.entities.Kategorie;
import de.thm.vs.efridge.lieferantenmanagement.data.entities.Lieferant;
import de.thm.vs.efridge.lieferantenmanagement.repository.LieferantenRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * Service Class zur Abarbeitung von Lieferanten.
 *
 */
@Service
@RequiredArgsConstructor
public class LieferantService {

    //Repository und Services (Dienste)
    private final LieferantenRepository lieferantenRepository;
    private final KategorienService kategorienService;

    /**
     * Abholen eines Lieferanten der einer Kategorie zugeordnet ist.
     *
     * @param kategorie Kategorie für die ein Lieferant gefunden werden soll
     * @return einen Lieferanten der die gegebene Kategorie zugewiesen hat oder NULL
     */
    public Lieferant retrieveLieferantByKategorie(Kategorie kategorie) {
        return lieferantenRepository.findByKategorien(kategorie);
    }

    /**
     * Legt eine Liste von Lieferanten in der Datenbank ab inklusive ihrer Kategorie, sofern diese nicht existiert.
     *
     * @param lieferanten Liste an Lieferanten die abgelegt werden sollen inklusiver ihrer Kategorie
     * @return Liste an Lieferanten die abgelegt wurden
     */
    public List<Lieferant> addLieferanten(List<Lieferant> lieferanten) {
        for(Lieferant l : lieferanten) {
            kategorienService.createKategorienIfNotExists(l.getKategorien());
        }
        return lieferantenRepository.saveAll(lieferanten);
    }

}
