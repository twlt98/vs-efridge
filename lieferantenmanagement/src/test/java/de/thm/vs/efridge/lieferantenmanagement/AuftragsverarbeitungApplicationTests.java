package de.thm.vs.efridge.lieferantenmanagement;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
class LieferantenmanagementApplicationTests {

	@Test
	void contextLoads() {
	}

}
