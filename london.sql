--
-- PostgreSQL database dump
--

-- Dumped from database version 16.1 (Debian 16.1-1.pgdg120+1)
-- Dumped by pg_dump version 16.1 (Debian 16.1-1.pgdg120+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: auftrag; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.auftrag (
    id bigint NOT NULL,
    anzahl integer,
    status character varying(255),
    kunde_id bigint,
    produkt_id bigint
);


ALTER TABLE public.auftrag OWNER TO "user";

--
-- Name: auftrag_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.auftrag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.auftrag_id_seq OWNER TO "user";

--
-- Name: auftrag_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.auftrag_id_seq OWNED BY public.auftrag.id;


--
-- Name: bestellung; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.bestellung (
    id bigint NOT NULL,
    bestellnummer character varying(255),
    created_at timestamp(6) without time zone,
    status character varying(255),
    bestellung_werk_id bigint,
    lieferant_id bigint
);


ALTER TABLE public.bestellung OWNER TO "user";

--
-- Name: bestellung_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.bestellung_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.bestellung_id_seq OWNER TO "user";

--
-- Name: bestellung_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.bestellung_id_seq OWNED BY public.bestellung.id;


--
-- Name: bestellung_produktmaterial; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.bestellung_produktmaterial (
    bestellung_id bigint NOT NULL,
    produktmaterial_id bigint NOT NULL
);


ALTER TABLE public.bestellung_produktmaterial OWNER TO "user";

--
-- Name: bestellung_werk; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.bestellung_werk (
    id bigint NOT NULL,
    anzahl integer,
    auftrag_id bigint,
    status character varying(255),
    werk character varying(255),
    produkt_id bigint
);


ALTER TABLE public.bestellung_werk OWNER TO "user";

--
-- Name: bestellung_werk_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.bestellung_werk_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.bestellung_werk_id_seq OWNER TO "user";

--
-- Name: bestellung_werk_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.bestellung_werk_id_seq OWNED BY public.bestellung_werk.id;


--
-- Name: kategorie; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.kategorie (
    name character varying(255) NOT NULL
);


ALTER TABLE public.kategorie OWNER TO "user";

--
-- Name: kunde; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.kunde (
    id bigint NOT NULL,
    adresse character varying(255),
    email character varying(255),
    name character varying(255),
    passwort character varying(255)
);


ALTER TABLE public.kunde OWNER TO "user";

--
-- Name: kunde_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.kunde_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.kunde_id_seq OWNER TO "user";

--
-- Name: kunde_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.kunde_id_seq OWNED BY public.kunde.id;


--
-- Name: lieferant; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.lieferant (
    id bigint NOT NULL,
    beschreibung character varying(255),
    name character varying(255)
);


ALTER TABLE public.lieferant OWNER TO "user";

--
-- Name: lieferant_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.lieferant_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.lieferant_id_seq OWNER TO "user";

--
-- Name: lieferant_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.lieferant_id_seq OWNED BY public.lieferant.id;


--
-- Name: lieferant_kategorie; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.lieferant_kategorie (
    lieferant_id bigint NOT NULL,
    kategorie character varying(255) NOT NULL
);


ALTER TABLE public.lieferant_kategorie OWNER TO "user";

--
-- Name: material; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.material (
    id bigint NOT NULL,
    beschreibung character varying(255),
    name character varying(255),
    preis double precision,
    kategorie character varying(255)
);


ALTER TABLE public.material OWNER TO "user";

--
-- Name: material_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.material_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.material_id_seq OWNER TO "user";

--
-- Name: material_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.material_id_seq OWNED BY public.material.id;


--
-- Name: produkt; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.produkt (
    id bigint NOT NULL,
    beschreibung character varying(255),
    dim character varying(255),
    gewicht double precision,
    name character varying(255),
    produktionszeit bigint
);


ALTER TABLE public.produkt OWNER TO "user";

--
-- Name: produkt_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.produkt_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.produkt_id_seq OWNER TO "user";

--
-- Name: produkt_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.produkt_id_seq OWNED BY public.produkt.id;


--
-- Name: produkt_material; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.produkt_material (
    id bigint NOT NULL,
    anzahl integer,
    material_id bigint,
    produkt_id bigint
);


ALTER TABLE public.produkt_material OWNER TO "user";

--
-- Name: produkt_material_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.produkt_material_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.produkt_material_id_seq OWNER TO "user";

--
-- Name: produkt_material_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.produkt_material_id_seq OWNED BY public.produkt_material.id;


--
-- Name: auftrag id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.auftrag ALTER COLUMN id SET DEFAULT nextval('public.auftrag_id_seq'::regclass);


--
-- Name: bestellung id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.bestellung ALTER COLUMN id SET DEFAULT nextval('public.bestellung_id_seq'::regclass);


--
-- Name: bestellung_werk id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.bestellung_werk ALTER COLUMN id SET DEFAULT nextval('public.bestellung_werk_id_seq'::regclass);


--
-- Name: kunde id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.kunde ALTER COLUMN id SET DEFAULT nextval('public.kunde_id_seq'::regclass);


--
-- Name: lieferant id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.lieferant ALTER COLUMN id SET DEFAULT nextval('public.lieferant_id_seq'::regclass);


--
-- Name: material id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.material ALTER COLUMN id SET DEFAULT nextval('public.material_id_seq'::regclass);


--
-- Name: produkt id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.produkt ALTER COLUMN id SET DEFAULT nextval('public.produkt_id_seq'::regclass);


--
-- Name: produkt_material id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.produkt_material ALTER COLUMN id SET DEFAULT nextval('public.produkt_material_id_seq'::regclass);


--
-- Data for Name: auftrag; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.auftrag (id, anzahl, status, kunde_id, produkt_id) FROM stdin;
\.


--
-- Data for Name: bestellung; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.bestellung (id, bestellnummer, created_at, status, bestellung_werk_id, lieferant_id) FROM stdin;
\.


--
-- Data for Name: bestellung_produktmaterial; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.bestellung_produktmaterial (bestellung_id, produktmaterial_id) FROM stdin;
\.


--
-- Data for Name: bestellung_werk; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.bestellung_werk (id, anzahl, auftrag_id, status, werk, produkt_id) FROM stdin;
\.


--
-- Data for Name: kategorie; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.kategorie (name) FROM stdin;
Metall
Glas
Kunststoff
Gummi
Verbundmaterial
Glass
\.


--
-- Data for Name: kunde; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.kunde (id, adresse, email, name, passwort) FROM stdin;
1	35423, ABCDEF, XYZ Str. 14	abc@mail.com	Max Mustermann	abcd
\.


--
-- Data for Name: lieferant; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.lieferant (id, beschreibung, name) FROM stdin;
1	Cool Mechanics Stuff	electrostuff
2	Cool Mechanics Stuff	coolmechanics
\.


--
-- Data for Name: lieferant_kategorie; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.lieferant_kategorie (lieferant_id, kategorie) FROM stdin;
1	Glass
1	Verbundmaterial
2	Metall
2	Kunststoff
2	Gummi
\.


--
-- Data for Name: material; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.material (id, beschreibung, name, preis, kategorie) FROM stdin;
1	Hochwertiger Edelstahl für Kühlgeräte	Edelstahl	100	Metall
2	Spezialglas für Kühlschranktüren	Glas	50	Glas
3	Robuster Kunststoff für Innenkomponenten	Kunststoff	20	Kunststoff
4	Leichtes Aluminium für Rahmen und Griffe	Aluminium	30	Metall
5	Flexibler Gummi für Dichtungen und Isolierungen	Gummi	10	Gummi
6	Glasfaser für verstärkte Innenstrukturen	Glasfaser	40	Verbundmaterial
7	Kupfer für elektrische Komponenten	Kupfer	60	Metall
8	Silikon für hitzebeständige Dichtungen	Silikon	15	Kunststoff
\.


--
-- Data for Name: produkt; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.produkt (id, beschreibung, dim, gewicht, name, produktionszeit) FROM stdin;
1	Kompakter Kühlschrank mit hoher Energieeffizienz	150cm * 50cm * 60cm	50.5	EFridge Modell A	20
2	Geräumiger Kühlschrank mit innovativer Kühltechnologie	200cm * 60cm * 70cm	75.2	EFridge Modell B	22
3	Stilvoller Kühlschrank mit Touchscreen-Bedienfeld	180cm * 55cm * 65cm	60	EFridge Modell C	24
4	Familienfreundlicher Kühlschrank mit extra großer Kapazität	210cm * 65cm * 75cm	82.3	EFridge Modell D	28
5	Energiesparender Kühlschrank mit umweltfreundlichem Design	165cm * 50cm * 60cm	55.4	EFridge Modell E	30
\.


--
-- Data for Name: produkt_material; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.produkt_material (id, anzahl, material_id, produkt_id) FROM stdin;
1	10	1	1
2	5	3	1
3	7	5	1
4	8	2	2
5	6	4	2
6	9	6	2
7	4	1	3
8	8	3	3
9	3	7	3
10	5	2	4
11	10	4	4
12	6	8	4
13	7	5	5
14	2	6	5
15	4	7	5
\.


--
-- Name: auftrag_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.auftrag_id_seq', 1, false);


--
-- Name: bestellung_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.bestellung_id_seq', 1, false);


--
-- Name: bestellung_werk_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.bestellung_werk_id_seq', 1, false);


--
-- Name: kunde_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.kunde_id_seq', 1, true);


--
-- Name: lieferant_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.lieferant_id_seq', 2, true);


--
-- Name: material_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.material_id_seq', 8, true);


--
-- Name: produkt_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.produkt_id_seq', 5, true);


--
-- Name: produkt_material_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.produkt_material_id_seq', 15, true);


--
-- Name: auftrag auftrag_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.auftrag
    ADD CONSTRAINT auftrag_pkey PRIMARY KEY (id);


--
-- Name: bestellung bestellung_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.bestellung
    ADD CONSTRAINT bestellung_pkey PRIMARY KEY (id);


--
-- Name: bestellung_werk bestellung_werk_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.bestellung_werk
    ADD CONSTRAINT bestellung_werk_pkey PRIMARY KEY (id);


--
-- Name: kategorie kategorie_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.kategorie
    ADD CONSTRAINT kategorie_pkey PRIMARY KEY (name);


--
-- Name: kunde kunde_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.kunde
    ADD CONSTRAINT kunde_pkey PRIMARY KEY (id);


--
-- Name: lieferant lieferant_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.lieferant
    ADD CONSTRAINT lieferant_pkey PRIMARY KEY (id);


--
-- Name: material material_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.material
    ADD CONSTRAINT material_pkey PRIMARY KEY (id);


--
-- Name: produkt_material produkt_material_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.produkt_material
    ADD CONSTRAINT produkt_material_pkey PRIMARY KEY (id);


--
-- Name: produkt produkt_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.produkt
    ADD CONSTRAINT produkt_pkey PRIMARY KEY (id);


--
-- Name: bestellung fk2svl56ncmcrq9ryjmlfewpso4; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.bestellung
    ADD CONSTRAINT fk2svl56ncmcrq9ryjmlfewpso4 FOREIGN KEY (lieferant_id) REFERENCES public.lieferant(id);


--
-- Name: lieferant_kategorie fk340gkvh4qj4s5rnfcakubugry; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.lieferant_kategorie
    ADD CONSTRAINT fk340gkvh4qj4s5rnfcakubugry FOREIGN KEY (kategorie) REFERENCES public.kategorie(name);


--
-- Name: produkt_material fk7fyu36eg4wmpf5xvpj6xag8c5; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.produkt_material
    ADD CONSTRAINT fk7fyu36eg4wmpf5xvpj6xag8c5 FOREIGN KEY (produkt_id) REFERENCES public.produkt(id);


--
-- Name: bestellung_werk fka8aj0flni64e5nxumlk8889en; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.bestellung_werk
    ADD CONSTRAINT fka8aj0flni64e5nxumlk8889en FOREIGN KEY (produkt_id) REFERENCES public.produkt(id);


--
-- Name: bestellung_produktmaterial fkcgm8dier9ijfdlsvfnealoaix; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.bestellung_produktmaterial
    ADD CONSTRAINT fkcgm8dier9ijfdlsvfnealoaix FOREIGN KEY (produktmaterial_id) REFERENCES public.produkt_material(id);


--
-- Name: bestellung fkcm014hlqxr5e2k8sfphaih99d; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.bestellung
    ADD CONSTRAINT fkcm014hlqxr5e2k8sfphaih99d FOREIGN KEY (bestellung_werk_id) REFERENCES public.bestellung_werk(id);


--
-- Name: auftrag fkdtmv1h8skil76ibamwksp4fk1; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.auftrag
    ADD CONSTRAINT fkdtmv1h8skil76ibamwksp4fk1 FOREIGN KEY (kunde_id) REFERENCES public.kunde(id);


--
-- Name: lieferant_kategorie fkeu33hpuhewebjlxlf4ybw0lw4; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.lieferant_kategorie
    ADD CONSTRAINT fkeu33hpuhewebjlxlf4ybw0lw4 FOREIGN KEY (lieferant_id) REFERENCES public.lieferant(id);


--
-- Name: bestellung_produktmaterial fkf7lmaqnq28yrwla7qt83svym; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.bestellung_produktmaterial
    ADD CONSTRAINT fkf7lmaqnq28yrwla7qt83svym FOREIGN KEY (bestellung_id) REFERENCES public.bestellung(id);


--
-- Name: produkt_material fkkpyj96g9d98efb0gicmb9rvjj; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.produkt_material
    ADD CONSTRAINT fkkpyj96g9d98efb0gicmb9rvjj FOREIGN KEY (material_id) REFERENCES public.material(id);


--
-- Name: material fkp5i5l718yqaow3rr1yrqcsagu; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.material
    ADD CONSTRAINT fkp5i5l718yqaow3rr1yrqcsagu FOREIGN KEY (kategorie) REFERENCES public.kategorie(name);


--
-- Name: auftrag fkqm7vta1haj7ulh2shqk0p0g47; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.auftrag
    ADD CONSTRAINT fkqm7vta1haj7ulh2shqk0p0g47 FOREIGN KEY (produkt_id) REFERENCES public.produkt(id);


--
-- PostgreSQL database dump complete
--

